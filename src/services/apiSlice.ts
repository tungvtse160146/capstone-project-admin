import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { UserLogin } from '../interfaces/SignIn';

export const apiSlice = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({
    baseUrl: 'https://chanhxe.fly.dev/api/internal',
    prepareHeaders: (headers) => {
      const token = localStorage.getItem('sessionId')
      if (token) {
        headers.set('Authorization', `Bearer ${token}`);
      }
      headers.set('Content-Type', 'application/json');
      headers.set('Accept', 'application/json')
      return headers;
    }
  }),
  endpoints: (builder) => ({
    signIn: builder.mutation<any, UserLogin>({
      query: (loginData) => ({
        url: '/login/token',
        method: "POST",
        body: loginData
      })
    }),
    getUser: builder.query<any, any>({
      query: (params) => ({
        url: `/account/customers`,
        params: {
          page: params.page,
          perPage: params.perPage
        }
      })

    }),
    getBoxSize: builder.query<any, any>({
      query: (params) => ({
        url: `/tariff/box-sizes`,
        params: {
          page: params.page,
          perPage: params.perPage
        }
      })

    })
  }),
})

export const { useGetUserQuery, useGetBoxSizeQuery, useSignInMutation, useLazyGetBoxSizeQuery } = apiSlice