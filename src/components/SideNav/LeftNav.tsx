import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import { Container, Sidebar, Sidenav, Content, Nav, DOMHelper } from 'rsuite';
import { Outlet } from 'react-router-dom';
import NavToggle from './NavToggle';
import Header from '../Header/Header';
import NavLink from '../NavLink';
import Brand from './Brand';
import { useDispatch, useSelector } from 'react-redux';
import { setTitle } from '../../store/navSideSlice';
import { RootState } from '../../store/store';
import Copyright from './CopyRight';

const { getHeight, on } = DOMHelper;

const NavItem = (props: any) => {
  const { title, eventKey, ...rest } = props;
  const dispatch = useDispatch()
  const currentTitle = useSelector((state: RootState) => state.title.title)

  return (
    <Nav.Item onClick={() => {
      dispatch(setTitle({
        title: title
      }))
    }} eventKey={eventKey} as={NavLink} {...rest}>
      <p style={{
        color: currentTitle === title ? '#3498ff' : 'black'
      }}>{title}</p>
    </Nav.Item>
  );
};

export interface NavItemData {
  eventKey: string;
  title: string;
  icon?: any;
  to?: string;
  target?: string;
  children?: NavItemData[];
}

export interface FrameProps {
  navs: NavItemData[];
  children?: React.ReactNode;
}

const Frame = (props: FrameProps) => {
  const { navs } = props;
  const [expand, setExpand] = useState(true);
  const [windowHeight, setWindowHeight] = useState(getHeight(window));

  useEffect(() => {
    setWindowHeight(getHeight(window));
    const resizeListenner = on(window, 'resize', () => setWindowHeight(getHeight(window)));

    return () => {
      resizeListenner.off();
    };
  }, []);

  const containerClasses = classNames('page-container', {
    'container-full': !expand,

  });

  const navBodyStyle: React.CSSProperties = expand
    ? { height: windowHeight - 112, overflow: 'auto' }
    : {};

  return (
    <Container style={{
    }} className="frame">
      <Sidebar
        style={{
          flexDirection: 'column',
          backgroundColor: 'white',
          display: 'flex',
          position: 'fixed', // Set the sidebar to be fixed
          left: 0, // Adjust the left position as needed
          top: 0, // Adjust the top position as needed
          height: '100%', // Make the sidebar take the full height of the viewport
          zIndex: 1000,
        }}
        width={expand ? 260 : 56}
        collapsible
      >
        <Sidenav.Header>
          <Brand expand={expand} />
        </Sidenav.Header>
        <Sidenav expanded={expand} appearance="subtle" defaultOpenKeys={['2', '3']}>
          <Sidenav.Body style={navBodyStyle}>
            <Nav>
              {navs.map(item => {
                const { children, ...rest } = item;
                if (children) {
                  return (
                    <Nav.Menu key={item.eventKey} placement="rightStart" trigger="hover" {...rest}>
                      {children.map(child => {
                        return <NavItem key={child.eventKey} {...child} />;
                      })}
                    </Nav.Menu>
                  );
                }

                if (rest.target === '_blank') {
                  return (
                    <Nav.Item key={item.eventKey} {...rest}>
                      {item.title}
                    </Nav.Item>
                  );
                }

                return <NavItem key={rest.eventKey} {...rest} />;
              })}
            </Nav>
          </Sidenav.Body>
        </Sidenav>
        <NavToggle expand={expand} onChange={() => setExpand(!expand)} />
      </Sidebar>

      <Container style={{ marginLeft: expand ? 260 : 56, backgroundColor: "#f5f8fa", transition: 'all 0.25s ease-out' }} className={containerClasses}>
        <Header expand={expand} />
        <Content style={{ marginTop: '5%', minHeight: 'calc(78vh)' }}>
          <Outlet />
        </Content>
        <Copyright />

      </Container>
    </Container >
  );
};

export default Frame;
