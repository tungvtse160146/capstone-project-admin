import { Typography } from '@mui/material';
import React from 'react';
import { Stack } from 'rsuite';
type Props = {
}

const Copyright = (props: Props) => {
    return (
        <Stack className="copyright" justifyContent="center" style={{ height: 40, zIndex: "999", backgroundColor: '#3EBDE0', paddingBottom: '3rem', paddingTop: '3rem' }}>
            <div className="container">
                <Typography textAlign={'center'} fontWeight={'bold'} color={'white'}>
                    Thực hiện bởi {' '}
                    CHÀNH XE ❤️
                </Typography>
                <Typography textAlign={'center'} fontWeight={'bold'} color={'white'}>
                    © 2023
                </Typography>
            </div>
        </Stack >
    );
};

export default Copyright;
