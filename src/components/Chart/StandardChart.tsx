
import React, { useRef, useEffect, } from "react";
import { init, getInstanceByDom } from "echarts";
import type { CSSProperties } from "react";
import type { ECharts, EChartsOption } from "echarts";

import { Box } from "@mui/material";



export interface ReactEChartsProps {
    style?: CSSProperties;
    // settings?: SetOptionOpts;
    trans?: boolean;
    loading?: boolean;
    option: EChartsOption;
    chartInstanceCallback?: (chartInstance: HTMLDivElement | null) => void;
}


export default function ReactECharts({
    option,
    style,
    // settings,
    loading,
    trans,
    chartInstanceCallback
}: ReactEChartsProps): JSX.Element {
    const chartRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        // Initialize chart
        let chart: ECharts | undefined;
        if (chartRef.current !== null) {
            chart = init(chartRef.current);
            chart?.on('click', function () {
                // const dispatch: AppDispatch = useDispatch();
                // const selected: tabTypes = "PieChart";
                // alert('clicked')
            })
        }

        // Add chart resize listener
        // ResizeObserver is leading to a bit janky UX
        function resizeChart() {
            chart?.resize();
        }
        window.addEventListener("resize", resizeChart);

        // Return cleanup function
        return () => {
            chart?.dispose();
            window.removeEventListener("resize", resizeChart);
        };
    }, []);

    useEffect(() => {
        // Update chart
        if (chartRef.current !== null) {
            const chart = getInstanceByDom(chartRef.current);
            chart?.setOption(option, trans);
            chart?.on('click', function (params) {
                if (params.targetType === 'axisLabel' && typeof params.value === "string") {
                }
            })
        }
    }, [option, trans]); // Whenever theme changes we need to add option and setting due to it being deleted in cleanup function

    useEffect(() => {
        // Update chart
        if (chartRef.current !== null) {
            const chart = getInstanceByDom(chartRef.current);
            // eslint-disable-next-line @typescript-eslint/no-unused-expressions
            loading === true ? chart?.showLoading() : chart?.hideLoading();
        }
    }, [loading]);

    useEffect(() => {
        if (chartRef.current !== null) {
            if (typeof chartInstanceCallback === "function") {
                chartInstanceCallback(chartRef.current);
            }
        }
    }, [chartInstanceCallback, option, trans, loading]);

    return <Box ref={chartRef} style={{ width: "100%", height: "100%", ...style }} />;
}

