import React, { useRef } from 'react';
import {
    Dropdown,
    Popover,
    Whisper,
    WhisperInstance,
    Stack,
    Badge,
    Avatar,
    IconButton,
    List,
    Button
} from 'rsuite';
import { useNavigate, Link } from 'react-router-dom';
import { Box, Typography } from '@mui/material';





type Props = {
    expand: Boolean,
}

const Header = (props: Props) => {
    const { expand } = props
    const trigger = useRef<WhisperInstance>(null);
    const navigate = useNavigate()
    const handleSelect = (eventKey: any) => {
    };
    const username = localStorage.getItem('username')
    const hub_name = localStorage.getItem('hubname')
    const role = localStorage.getItem('role')
    return (
        <Box display={'flex'} position={'fixed'} gap='1rem' zIndex={999} sx={{
            transition: 'width 0.25s ease-in-out',
            width: expand ? '81.5vw' : '95.2vw'
        }} py="1rem" justifyContent={'flex-end'} alignItems={'center'} pr="0.25rem" bgcolor={'#fff'} >
            <Typography fontWeight={"bold"}>Xin chào, {username?.toLocaleUpperCase()} {role === 'staff' && `, ${hub_name}`}</Typography>
            <Whisper placement="bottomEnd" trigger="click" ref={trigger} speaker={(<Popover full>
                <Dropdown.Menu onSelect={handleSelect}>
                    <Dropdown.Item panel style={{ padding: 10, width: 160 }}>
                        <p>Đăng nhập bởi</p>
                        <strong>{username}</strong>
                    </Dropdown.Item>
                    <Dropdown.Item divider />
                    <Dropdown.Item onClick={() => {
                        navigate("change-password")
                    }}>
                        Quản lý tài khoản</Dropdown.Item>
                    {role === 'staff' && (
                        <Dropdown.Item onClick={() => {
                            navigate("profile")
                        }}>
                            Thông tin nhân viên</Dropdown.Item>
                    )}
                    <Dropdown.Item divider />
                    <Dropdown.Item onClick={() => {
                        localStorage.removeItem("sessionId")
                        localStorage.removeItem('role')
                        navigate("/login")
                    }}>Đăng xuất</Dropdown.Item>

                </Dropdown.Menu>
            </Popover>)}>
                <Avatar
                    size="sm"
                    circle
                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXTdolvwJEJdsHZTJI6F7LjUDXeMidz0PPkQ&usqp=CAU"
                    alt="@simonguo"
                    style={{ marginLeft: 8 }}
                />
            </Whisper>
        </Box>
    );
};

export default Header;
