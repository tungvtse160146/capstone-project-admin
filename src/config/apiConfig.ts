import Axios from 'axios';

import { url } from "../url";

const axios = Axios.create({
  baseURL: url,
  headers: {
    'Content-Type': 'application/json',
    "Accept": "application/json",
  },
});

axios.interceptors.request.use(async (config) => {
  const bearerToken = localStorage.getItem('sessionId')

  config.headers["Authorization"] = "Bearer " + bearerToken;
  return config;
});

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default axios;
