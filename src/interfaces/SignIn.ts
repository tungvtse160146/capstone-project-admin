export type UserLogin = {
    username: string,
    password: string,
    device_name: string,
}