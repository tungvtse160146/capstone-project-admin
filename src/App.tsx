import React from 'react';
import logo from './logo.svg';
import './App.css';
import enGB from 'rsuite/locales/en_GB';
import { IntlProvider } from 'react-intl';
import { CustomProvider } from 'rsuite';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { adminNavs, staffNavs } from './config';
import LoginPage from './pages/sign-in';
import Frame from './components/SideNav/LeftNav';
import OrderTable from './pages/tables/order';
import BoxSizeTable from './pages/tables/boxSize';
import PartnerAccount from './pages/account/PartnerAccount';
import StaffAccount from './pages/account/StaffAccount';
import { useSelector } from 'react-redux';
import { RootState } from './store/store';
import StaffsTable from './pages/tables/staff';
import UserPage from './pages/tables/user';
import ChangePassword from './pages/changePassword';
import PartnerTable from './pages/tables/partner';
import TransactionTable from './pages/tables/transaction';
import StationTable from './pages/tables/stations';
import Dashboard from './pages/dashboard';
import Payment from './pages/tables/payment';
import PaymentPartnerTable from './pages/tables/paymentPartner';
import Profile from './pages/sign-in/Profile';
import PaymentTable from './pages/tables/payment';
import HubTable from './pages/hub';


function App() {

  const [currentRole, setCurrentRole] = React.useState<string>('')

  const role = useSelector((state: RootState) => state.role.role)
  const localRole = localStorage.getItem('role')
  React.useEffect(() => {
    if (role === null && localRole) {
      setCurrentRole(localRole)
    }
    else if (role) {
      setCurrentRole(role)
    }
  }, [role])

  return (
    <IntlProvider locale="en" >
      <CustomProvider locale={enGB} >
        <BrowserRouter>
          <Routes>
            <Route path="/login" element={<LoginPage />} />
            <Route path="/" element={<Frame navs={currentRole === 'admin' ? adminNavs : staffNavs} />}>
              <Route index element={currentRole === 'admin' ? <Dashboard /> : <OrderTable />} />
              <Route path="orders" element={<OrderTable />} />
              {currentRole === 'admin' && (
                <>
                  <Route path="dashboards" element={<Dashboard />} />
                  <Route path="create-partner-account" element={<PartnerAccount />} />
                  <Route path="create-staff-account" element={<StaffAccount />} />
                  <Route path="box-size" element={<BoxSizeTable />} />
                  <Route path="staffs" element={<StaffsTable />} />
                  <Route path="proceed-station" element={<StationTable />} />
                  <Route path="users" element={<UserPage />} />
                  <Route path="payments" element={<PaymentTable />} />
                  <Route path="hubs" element={<HubTable />} />
                </>
              )}
              <Route path="change-password" element={<ChangePassword />} />
              <Route path="partners" element={<PartnerTable />} />
              <Route path="transactions" element={<TransactionTable />} />
              <Route path="profile" element={<Profile />} />
            </Route>
          </Routes>
        </BrowserRouter>


      </CustomProvider>
    </IntlProvider >


  );
}

export default App;
