import React from 'react'
import { url } from '../../url'
import axios from 'axios'
import { setRole } from '../../store/roleSlice'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'


export const login = async (values: any, formSubmitProps: any) => {

    const [error, setError] = React.useState()
    const [data, setData] = React.useState()
    const [isLoading, setIsLoading] = React.useState(false)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json"
    }
    try {
        setIsLoading(true)
        axios({
            method: 'post',
            url: `${url}/login/token`,
            data: JSON.stringify(values),
            headers: headers
        }).then((res) => {
            if (res.status === 200) {
                localStorage.setItem('sessionId', res.data.data.access_token)
                localStorage.setItem('username', res.data.data.staff.username)
                const headers = {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "Authorization": `Bearer ${res.data.data.access_token}`
                }
                try {
                    axios({
                        method: 'get',
                        headers: headers,
                        url: `${url}/profile/me`
                    }).then((res) => {
                        localStorage.setItem('role', res.data.data.type)
                        dispatch(setRole({
                            role: res.data.data.type
                        }))
                        setData(res.data.data)

                    }).catch((error) => {
                        console.log(error);

                    })
                } catch (error) {
                    console.log(error);

                }
                navigate("/")
            }
            setIsLoading(false)


        }).catch((error) => {

            setError(error.response.status)



        })
        formSubmitProps.resetForm()
    } catch (error) {
        console.log(error);

    }
    return { data, error, isLoading }

}