import axios from 'axios'
import React from 'react'
import { url } from '../../url'
import { Box, Typography } from '@mui/material'
import { Loader } from 'rsuite'

type Props = {}

type Profile = {
    id: string,
    type: string,
    username: string,
    email: string,
    created_at: string,
    updated_at: string,
    hubId?: string,
    hubName?: string,
    address: string,
}

const Profile = (props: Props) => {
    const [profile, setProfile] = React.useState<Profile>()
    const [isLoading, setIsLoading] = React.useState(false)
    const bearerToken = localStorage.getItem('sessionId')
    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`
    }
    const getProfile = async () => {

        try {
            setIsLoading(true)
            axios({
                method: 'get',
                url: `${url}/profile/me?include=hub`,
                headers: headers
            }).then((res) => {
                if (res) {
                    let data = res.data.data
                    const dataResponse = {
                        id: data.id,
                        type: data.type,
                        username: data.attributes.username,
                        email: data.attributes.email,
                        created_at: data.attributes.created_at,
                        updated_at: data.attributes.updated_at,
                        hubId: data.relationships.hub.data.id,
                        hubName: res.data.included[0].attributes.name,
                        address: res.data.included[0].attributes.address
                    }
                    setProfile(dataResponse)
                    setIsLoading(false)
                }
            }).catch((error) => {
                console.log(error);
                setIsLoading(false)
            })
        } catch (error) {
            console.log(error);
            setIsLoading(false)
        }
    }

    React.useEffect(() => {
        getProfile()
    }, [])
    return (
        <>
            {!isLoading && profile ? (
                <Box p="1rem">
                    <Box >
                        <Typography pb="1rem" fontWeight={'bold'} variant='h6' color="black">Thông tin nhân viên</Typography>
                    </Box>
                    <Box display={'flex'} gap="0.5rem" >
                        <Box display={'flex'} flexBasis={'30%'} flexDirection={'column'} gap="0.5rem" bgcolor={'white'} justifyContent={'center'} alignItems={'center'}>
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXTdolvwJEJdsHZTJI6F7LjUDXeMidz0PPkQ&usqp=CAU" style={{
                                height: '200px', width: '200px', borderRadius: '50%'
                            }} />
                            <Typography fontWeight={'bold'}>
                                {profile.username}
                            </Typography>

                        </Box>
                        <Box flexBasis={'70%'} bgcolor={'white'} p="1rem" justifyContent={'flex-start'}>
                            <Box display={'flex'} >
                                <Box flexBasis={'50%'} display={'flex'} flexDirection={'column'} justifyContent={'flex-start'}>
                                    <Typography fontWeight={'bold'}>Tên đăng nhập: </Typography>
                                    <Typography fontWeight={'bold'}>Email: </Typography>
                                    <Typography fontWeight={'bold'}>Chức năng: </Typography>
                                    {
                                        profile.type === 'staff' && (
                                            <>
                                                <Typography fontWeight={'bold'}>Hub Id: </Typography>
                                                <Typography fontWeight={'bold'}>Tên hub: </Typography>
                                                <Typography fontWeight={'bold'}>Địa chỉ: </Typography>
                                            </>
                                        )
                                    }
                                    <Typography fontWeight={'bold'}>Ngày tham gia: </Typography>
                                </Box>
                                <Box flexBasis={'50%'} display={'flex'} flexDirection={'column'} justifyContent={'flex-start'}>
                                    <Typography >{profile.username} </Typography>
                                    <Typography >{profile.email} </Typography>
                                    <Typography >{profile.type} </Typography>
                                    {
                                        profile.type === 'staff' && (
                                            <>
                                                <Typography >{profile.hubId} </Typography>
                                                <Typography >{profile.hubName} </Typography>
                                                <Typography >{profile.address} </Typography>

                                            </>
                                        )
                                    }
                                    <Typography >{profile.created_at} </Typography>
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            ) : (<Loader center title='Đang tải dữ liệu' />)}
        </>
    )
}

export default Profile