import { Box } from '@mui/material'
import React, { useEffect } from 'react'
import ReactECharts from '../../../components/Chart/StandardChart'
import { EChartsOption } from 'echarts'
import axios from 'axios'
import { url } from '../../../url'
import { Loader } from 'rsuite'

type Props = {
}

type Money = {
    month: number,
    amount: number
}



const ProfitChart = (props: Props) => {
    const bearerToken = localStorage.getItem('sessionId')
    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`
    }
    const [profit, setProfit] = React.useState<Money[]>([])
    const [options, setOptions] = React.useState<any>()




    const getOrderRevenue = async () => {
        try {
            axios({
                url: `${url}/balance/order-revenue?year=2024`,
                headers: headers,
                method: 'get'
            }).then((res) => {
                if (res) {
                    setProfit(res.data.data)
                }

            }).catch((error) => {
                console.log(error);
            })
        } catch (error) {
            console.log(error);

        }
    }

    useEffect(() => {
        getOrderRevenue()
    }, [])



    useEffect(() => {
        if (profit) {
            const option = {
                title: {
                    text: 'Doanh thu trên đơn năm 2024',
                    textStyle: {
                        fontFamily: 'Roboto'
                    },
                    top: '2%',
                    left: '2.5%',

                },
                tooltip: {
                    trigger: 'axis'
                },
                grid: {
                    left: '4%',
                    right: '4%',
                    bottom: '5%',
                    containLabel: true
                },
                xAxis: {
                    type: 'category',
                    data: profit.map((item) => `Th.${item.month}`)
                },
                yAxis: {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value} VNĐ'
                    }
                },
                series: [
                    {
                        data: profit.map((item) => item.amount),
                        type: 'line',
                        color: '#00CED1'
                    }

                ]
            };
            setOptions(option)
        }
    }, [profit])


    return (
        <>
            {options ? (
                <Box bgcolor={'white'} width="100%" height="450px" display={'flex'} alignItems={'center'} justifyContent={'center'}>
                    <ReactECharts option={options} />
                </Box>
            ) : (
                <Loader center />
            )}
        </>

    )
}

export default ProfitChart