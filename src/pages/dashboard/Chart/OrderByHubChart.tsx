import { Box } from '@mui/material'
import React, { useEffect } from 'react'
import ReactECharts from '../../../components/Chart/StandardChart'
import { EChartsOption } from 'echarts'

type Hub = {
    hubId: string,
    hubName: string,
    numberOfOrders: number
}

type Props = {
    hub: Hub[]
}

const OrderByHubChart = (props: Props) => {
    const { hub } = props
    console.log(hub, 'hub');

    const option: EChartsOption = {
        title: {
            text: 'Đơn hàng theo hub',
            textStyle: {
                fontFamily: 'Roboto',
                fontSize: 20
            },

            top: '5%',
            left: '5%'

        },
        tooltip: {
            trigger: 'item'
        },
        legend: {
            left: 'center',
            bottom: '5%'
        },
        series: [
            {
                name: 'Access From',
                type: 'pie',
                radius: '50%',
                data: hub.map((item) => {
                    return {
                        value: item.numberOfOrders,
                        name: item.hubName
                    }
                }),
                center: ['50%', '50%'],
                labelLine: {
                    show: false
                },
                label: {
                    show: false
                },
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                color: ['#FFA07A', '#00CED1', '#864AF9', '#F8E559'],

            }
        ]
    };


    return (
        <Box bgcolor={'white'} width="100%" height="450px" display={'flex'} alignItems={'center'} justifyContent={'center'}>
            <ReactECharts option={option} />
        </Box>
    )
}

export default OrderByHubChart