import { Box } from '@mui/material'
import React from 'react'
import ReactECharts from '../../../components/Chart/StandardChart'
import { EChartsOption } from 'echarts'

type Props = {
    straightOrder: number,
    hubOrder: number
}

const TransactionChart = (props: Props) => {
    const { straightOrder, hubOrder } = props
    const option: EChartsOption = {
        title: {
            text: 'Số lượng đơn hàng',
            textStyle: {
                fontFamily: 'Roboto',
                fontSize: 20
            },

            top: '5%',
            left: '5%'

        },
        tooltip: {
            trigger: 'item'
        },
        legend: {
            left: 'center',
            bottom: '5%'
        },
        series: [
            {
                name: 'Access From',
                type: 'pie',
                radius: '50%',
                data: [
                    { value: hubOrder, name: 'Đơn hàng qua hub' },
                    { value: straightOrder, name: 'Đơn hàng đi thẳng' },

                ],
                center: ['50%', '50%'],
                labelLine: {
                    show: false
                },
                label: {
                    show: false
                },
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                color: ['#FFA07A', '#00CED1'],

            }
        ]
    };


    return (
        <Box bgcolor={'white'} width="100%" height="450px" display={'flex'} alignItems={'center'} justifyContent={'center'}>
            <ReactECharts option={option} />
        </Box>
    )
}

export default TransactionChart