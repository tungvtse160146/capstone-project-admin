import { Box } from '@mui/material'
import React, { useEffect } from 'react'
import ReactECharts from '../../../components/Chart/StandardChart'
import axios from 'axios'
import { url } from '../../../url'
import { Loader } from 'rsuite'

type Props = {
}

type Money = {
    month: number,
    amount: number
}



const RevenueChart = (props: Props) => {
    const bearerToken = localStorage.getItem('sessionId')
    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`
    }
    const [partnerTopUp, setPartnerTopUp] = React.useState<Money[]>([])
    const [partnerWithdraw, setPartnerWithdraw] = React.useState<Money[]>([])
    const [customerPayment, setCustomerPayment] = React.useState<Money[]>([])
    const [revenue, setRevenue] = React.useState<Money[]>([])
    const [options, setOptions] = React.useState<any>()


    const getPartnerTopUp = async () => {
        try {
            axios({
                url: `${url}/balance/partner-topup?year=2024`,
                headers: headers,
                method: 'get'
            }).then((res) => {
                if (res) {
                    setPartnerTopUp(res.data.data)
                    console.log(res, 'thuchi');

                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (error) {
            console.log(error);

        }
    }

    const getPartnerWithDraw = async () => {
        try {
            axios({
                url: `${url}/balance/partner-withdraw?year=2024`,
                headers: headers,
                method: 'get'
            }).then((res) => {
                if (res) {
                    setPartnerWithdraw(res.data.data)
                    console.log(res.data.data, 'rut tien');

                }

            }).catch((error) => {
                console.log(error);
            })
        } catch (error) {
            console.log(error);

        }
    }

    const getCustomerPayment = async () => {
        try {
            axios({
                url: `${url}/balance/customer-direct-payment?year=2024`,
                headers: headers,
                method: 'get'
            }).then((res) => {
                if (res) {
                    console.log(res.data.data, 'ajdfdsfnjskd');

                    setCustomerPayment(res.data.data)
                }

            }).catch((error) => {
                console.log(error);
            })
        } catch (error) {
            console.log(error);

        }
    }





    useEffect(() => {
        getPartnerTopUp()
        getCustomerPayment()
        getPartnerWithDraw()
    }, [])

    useEffect(() => {
        if (partnerTopUp.length && customerPayment.length && partnerWithdraw.length) {
            let revenue: Money[] = partnerTopUp.map((item, index) => {
                console.log(item.amount, customerPayment[index].amount, 'aaa');

                return {
                    month: item.month,
                    amount: item.amount + customerPayment[index].amount
                }

            })
            setRevenue(revenue)

        }
    }, [partnerTopUp, customerPayment, partnerWithdraw])

    useEffect(() => {
        if (revenue) {
            const option = {
                title: {
                    text: 'Số liệu thu chi năm 2024',
                    top: '2%',
                    left: '2.5%',
                    textStyle: {
                        fontFamily: 'Roboto'
                    },
                },
                legend: {
                    data: ['Khoản thu', 'Khoản chi', 'Biến động'],
                    top: '8%'
                },
                tooltip: {
                    trigger: 'axis'
                },
                grid: {
                    left: '4%',
                    right: '4%',
                    bottom: '5%',
                    containLabel: true
                },
                xAxis: {
                    type: 'category',
                    data: revenue.map((item) => `Th.${item.month}`)
                },
                yAxis: {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value} VNĐ'
                    }
                },
                series: [
                    {
                        emphasis: {
                            focus: 'series'
                        },
                        name: 'Khoản thu',
                        data: revenue.map((item) => item.amount),
                        type: 'bar',
                        color: '#00CED1',
                        stack: 'Total',

                    },
                    {
                        emphasis: {
                            focus: 'series'
                        },
                        name: 'Khoản chi',
                        data: partnerWithdraw.map((item) => item.amount),
                        type: 'bar',
                        color: 'red',
                        stack: 'Total',

                    },
                    {
                        emphasis: {
                            focus: 'series'
                        },
                        name: 'Biến động',
                        data: revenue.map((item, i) =>
                            item.amount + partnerWithdraw[i].amount
                        ),
                        type: 'bar',
                        color: '#FFA07A'
                    }

                ]
            };
            setOptions(option)
        }
    }, [revenue])


    return (
        <>
            {options ? (
                <Box bgcolor={'white'} width="100%" height="450px" display={'flex'} alignItems={'center'} justifyContent={'center'}>
                    <ReactECharts option={options} />
                </Box>
            ) : (
                <Loader center />
            )}
        </>

    )
}

export default RevenueChart