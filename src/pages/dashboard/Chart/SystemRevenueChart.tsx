import React, { useEffect, useState } from 'react'
import ReactECharts from '../../../components/Chart/StandardChart'
import { url } from '../../../url'
import axios from 'axios'
import { Box } from '@mui/material'
import { Loader } from 'rsuite'


type Props = {}

type SystemRevenue = {
    month: number,
    company_revenue: number,
    partner_revenue: number,
    system_revenue: number
}

const SystemRevenueChart = (props: Props) => {

    const [systemData, setSystemData] = useState<SystemRevenue[]>([])
    const [options, setOptions] = React.useState<any>()


    const getData = async () => {
        const bearerToken = localStorage.getItem('sessionId')
        const headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": `Bearer ${bearerToken}`
        }
        try {
            axios({
                url: `${url}/statistics/revenue/yearly?year=2024`,
                headers: headers,
                method: 'get'
            }).then((res) => {
                if (res) {
                    setSystemData(res.data.data)
                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (error) {
            console.log(error);

        }
    }

    useEffect(() => {
        getData()
    }, [])

    useEffect(() => {
        if (systemData) {
            const option = {
                title: {
                    text: 'Doanh thu hệ thống năm 2024',
                    top: '2%',
                    left: '2.5%',
                    textStyle: {
                        fontFamily: 'Roboto'
                    },
                },
                legend: {
                    data: ['Doanh thu hệ thống', 'Doanh thu đối tác', 'Doanh thu công ty'],
                    top: '8%'
                },
                tooltip: {
                    trigger: 'axis'
                },
                grid: {
                    left: '4%',
                    right: '4%',
                    bottom: '5%',
                    containLabel: true
                },
                xAxis: {
                    type: 'category',
                    data: systemData.map((item) => `Th.${item.month}`)
                },
                yAxis: {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value} VNĐ'
                    }
                },
                series: [
                    {
                        name: 'Doanh thu hệ thống',
                        data: systemData.map((item) => item.system_revenue),
                        type: 'bar',
                        color: '#00CED1'
                    },
                    {
                        name: 'Doanh thu đối tác',
                        data: systemData.map((item) => item.partner_revenue),
                        type: 'bar',
                        color: '#FFA07A'
                    },
                    {
                        name: 'Doanh thu công ty',
                        data: systemData.map((item) => item.company_revenue),
                        type: 'bar',
                        color: 'red'
                    }

                ]
            };
            setOptions(option)
        }
    }, [systemData])

    return (
        <>
            {options ? (
                <Box bgcolor={'white'} width="100%" height="450px" display={'flex'} alignItems={'center'} justifyContent={'center'}>
                    <ReactECharts option={options} />
                </Box>
            ) : (
                <Loader center />
            )}
        </>
    )
}

export default SystemRevenueChart