import { Box } from '@mui/material'
import React, { useEffect } from 'react'
import ReactECharts from '../../../components/Chart/StandardChart'
import { EChartsOption } from 'echarts'
import axios from 'axios'
import { url } from '../../../url'
import { Loader } from 'rsuite'

type Props = {
}

type Money = {
    month: number,
    amount: number
}

const DunoChart = (props: Props) => {
    const bearerToken = localStorage.getItem('sessionId')
    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`
    }
    const [partnerBalance, setPartnerBalance] = React.useState<Money[]>([])
    const [options, setOptions] = React.useState<any>()

    const getPartnerBalance = async () => {
        try {
            axios({
                url: `${url}/balance/partner-account-balance?year=2024`,
                headers: headers,
                method: 'get'
            }).then((res) => {
                if (res) {
                    setPartnerBalance(res.data.data)
                    console.log(res.data.data, 'duno');
                }

            }).catch((error) => {
                console.log(error);
            })
        } catch (error) {
            console.log(error);

        }
    }


    useEffect(() => {
        getPartnerBalance()
    }, [])


    useEffect(() => {
        if (partnerBalance) {
            const option = {
                title: {
                    text: 'Dư nợ năm 2024',
                    top: '2%',
                    left: '2.5%',
                    textStyle: {
                        fontFamily: 'Roboto'
                    },
                },
                tooltip: {
                    trigger: 'axis'
                },
                grid: {
                    left: '4%',
                    right: '4%',
                    bottom: '5%',
                    containLabel: true
                },
                xAxis: {
                    type: 'category',
                    data: partnerBalance.map((item) => `Th.${item.month}`)
                },
                yAxis: {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value} VNĐ'
                    }
                },
                series: [
                    {
                        data: partnerBalance.map((item) => item.amount),
                        type: 'bar',
                        color: '#00CED1'
                    }

                ]
            };
            setOptions(option)
        }
    }, [partnerBalance])


    return (
        <>
            {options ? (
                <Box bgcolor={'white'} width="100%" height="450px" display={'flex'} alignItems={'center'} justifyContent={'center'}>
                    <ReactECharts option={options} />
                </Box>
            ) : (
                <Loader center />
            )}
        </>

    )
}

export default DunoChart