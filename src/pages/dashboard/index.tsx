import { Box, Typography } from '@mui/material'
import React, { useEffect } from 'react'
import TopUser from './topUser'
import TransactionChart from './Chart/OrderChart'
import axios from 'axios'
import { url } from '../../url'
import { Loader } from 'rsuite'
import TopPartner from './topPartner'
import OrderByHubChart from './Chart/OrderByHubChart'
import useMediaQuery from '@mui/material/useMediaQuery';
import RevenueChart from './Chart/RevenueChart'
import ProfitChart from './Chart/ProfitChart'
import SystemRevenueChart from './Chart/SystemRevenueChart'


type Props = {}

type OrderByHub = {
    hubId: string,
    hubName: string,
    numberOfOrders: number
}

const Dashboard = (props: Props) => {
    const [numOfCustomer, setNumOfCustomer] = React.useState(0)
    const [isLoadingCustomer, setIsLoadingCustomer] = React.useState(false)
    const [straightOrder, setStraightOrder] = React.useState(0)
    const [hubOrder, setHubOrder] = React.useState(0)
    const [numOfOrders, setNumOfOrders] = React.useState(0)
    const [isLoadingOrder, setIsLoadingOrder] = React.useState(false)
    const [numOfPartners, setNumOfPartners] = React.useState(0)
    const [isLoadingPartner, setIsLoadingPartner] = React.useState(false)
    const [partnerBalance, setPartnerBalance] = React.useState(0)
    const [orderByHub, setOrderByHub] = React.useState<OrderByHub[]>([])
    const [systemRevenue, setSystemRevenue] = React.useState(0)
    const bearerToken = localStorage.getItem('sessionId')
    const isNonMobile = useMediaQuery("(min-width:600px)");


    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`
    }
    useEffect(() => {
        try {
            setIsLoadingCustomer(true)
            axios({
                method: 'get',
                url: `${url}/statistics/customers`,
                headers: headers
            }).then((res) => {
                setNumOfCustomer(res.data.data.number_of_customers)
                setIsLoadingCustomer(false)
            }).catch((error) => {
                console.log(error);
                setIsLoadingCustomer(false)

            })
        } catch (error) {
            console.log(error);
            setIsLoadingCustomer(false)

        }
        try {
            setIsLoadingOrder(true)
            axios({
                method: 'get',
                url: `${url}/statistics/orders`,
                headers: headers
            }).then((res) => {
                let number_of_straight_orders = res.data.data.number_of_straight_orders
                console.log('resjib', res.data.data);

                let hub_orders: any[] = res.data.data.hub_orders.map((item: any) => {
                    return {
                        hubName: item.hubName,
                        hubId: item.hubId,
                        numberOfOrders: item.numberOfOrders
                    }
                })
                setOrderByHub(hub_orders)
                let sum = 0
                for (let i = 0; i < hub_orders.length; i++) {
                    sum = sum + hub_orders[i].numberOfOrders
                }
                setStraightOrder(number_of_straight_orders)
                setHubOrder(sum)
                setNumOfOrders(sum + number_of_straight_orders)

                setIsLoadingOrder(false)
            }).catch((error) => {
                console.log(error);
                setIsLoadingOrder(false)

            })
        } catch (error) {
            console.log(error);
            setIsLoadingOrder(false)

        }
        try {
            setIsLoadingPartner(true)
            axios({
                method: 'get',
                url: `${url}/statistics/partners`,
                headers: headers
            }).then((res) => {
                setNumOfPartners(res.data.data.number_of_active_partners)
                setIsLoadingPartner(false)
            }).catch((error) => {
                console.log(error);
                setIsLoadingPartner(false)
            })
        } catch (error) {
            console.log(error);
            setIsLoadingPartner(false)
        }
        try {
            setIsLoadingPartner(true)
            axios({
                method: 'get',
                url: `${url}/balance/partner-account-balance`,
                headers: headers
            }).then((res) => {
                setPartnerBalance(res.data.data.amount)
                setSystemRevenue(res.data.data.order_revenue)
            }).catch((error) => {
                console.log(error);
                setIsLoadingPartner(false)
            })
        } catch (error) {
            console.log(error);
            setIsLoadingPartner(false)
        }
        // try {
        //     setIsLoadingPartner(true)
        //     axios({
        //         method: 'get',
        //         url: `${url}/balance/payments?per_page=1000`,
        //         headers: headers
        //     }).then((res) => {
        //         setSystemRevenue(res.data.data.amount)
        //     }).catch((error) => {
        //         console.log(error);
        //         setIsLoadingPartner(false)
        //     })
        // } catch (error) {
        //     console.log(error);
        //     setIsLoadingPartner(false)
        // }
    }, [])

    return (
        <Box m="1rem" display={'flex'} flexDirection={'column'} gap="1rem">
            <Box display={'flex'} gap="0.5rem" mb="1rem" flexDirection={isNonMobile ? 'row' : 'column'} >
                <Box flex="1" p="1rem" bgcolor={'white'} display={'flex'} justifyContent={'center'} alignItems={'center'} flexDirection={'column'} gap="0.5rem">
                    <Typography fontWeight={'bold'} color="black">Doanh thu hệ thống</Typography>
                    {!isLoadingCustomer && numOfCustomer ? (
                        <Typography fontWeight={'bold'} variant='h5' color={'#3EBDE0'}>{systemRevenue?.toLocaleString('en-us') || 0} VNĐ</Typography>
                    ) : (<Loader />)}
                </Box>
                <Box flex="1" p="1rem" py="2rem" bgcolor={'white'} display={'flex'} justifyContent={'center'} alignItems={'center'} flexDirection={'column'} gap="0.5rem">
                    <Typography fontWeight={'bold'} color="black">Dư nợ hiện tại</Typography>
                    {!isLoadingCustomer && numOfCustomer ? (
                        <Typography fontWeight={'bold'} variant='h5' color={'#3EBDE0'}>{partnerBalance.toLocaleString('en-us')} VNĐ</Typography>
                    ) : (<Loader />)}
                </Box>
                <Box flex="1" p="1rem" py="2rem" bgcolor={'white'} display={'flex'} justifyContent={'center'} alignItems={'center'} flexDirection={'column'} gap="0.5rem">
                    <Typography fontWeight={'bold'} color="black">Tổng số khách hàng</Typography>
                    {!isLoadingCustomer && numOfCustomer ? (
                        <Typography fontWeight={'bold'} variant='h4' color={'#3EBDE0'}>{numOfCustomer}</Typography>
                    ) : (<Loader />)}
                </Box>
                <Box flex="1" p="1rem" py="2rem" bgcolor={'white'} display={'flex'} justifyContent={'center'} alignItems={'center'} flexDirection={'column'} gap="0.5rem">
                    <Typography fontWeight={'bold'} color="black">Tổng số đơn hàng</Typography>
                    {!isLoadingOrder && numOfOrders !== null ? (
                        <Typography fontWeight={'bold'} variant='h4' color={'#3EBDE0'}>{numOfOrders}</Typography>

                    ) : (<Loader />)}
                </Box>
                <Box flex="1" p="1rem" py="2rem" bgcolor={'white'} display={'flex'} justifyContent={'center'} alignItems={'center'} flexDirection={'column'} gap="0.5rem">
                    <Typography fontWeight={'bold'} color="black">Tổng số đối tác</Typography>
                    {!isLoadingPartner && numOfPartners !== null ? (
                        <Typography fontWeight={'bold'} variant='h4' color={'#3EBDE0'}>{numOfPartners}</Typography>

                    ) : (<Loader />)}
                </Box>
            </Box>
            <Box display={'flex'} gap="0.5rem" >

                <RevenueChart />

                <ProfitChart />



            </Box>
            <Box>
            </Box>
            <Box>
                <SystemRevenueChart />
            </Box>

            <Box display={'flex'} gap="0.5rem" flexDirection={isNonMobile ? 'row' : 'column'}>

                <Box flexBasis={'50%'} >
                    <TopUser />
                </Box>
                <Box flexBasis={'50%'}>
                    <TopPartner />
                </Box>

            </Box>
            <Box my="1rem" display={'flex'} gap="1rem" flexDirection={isNonMobile ? 'row' : 'column'} >
                <Box flexBasis={isNonMobile ? '50%' : '100%'} >
                    {!isLoadingOrder ? (
                        <TransactionChart straightOrder={straightOrder} hubOrder={hubOrder} />

                    ) : (<Loader />)}

                </Box>
                <Box flexBasis={isNonMobile ? '50%' : '100%'} >
                    {!isLoadingOrder && orderByHub.length ? (
                        <OrderByHubChart hub={orderByHub} />

                    ) : (<Loader />)}

                </Box>
            </Box>

        </Box>
    )
}

export default Dashboard