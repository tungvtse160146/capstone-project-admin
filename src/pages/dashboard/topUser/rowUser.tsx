import { Box, Typography, useMediaQuery } from '@mui/material'
import React from 'react'

type Props = {
    name: string,
    email: string,
    numOfOrder: number
    index: number
}

const RowUser = (props: Props) => {
    const { name, email, numOfOrder, index } = props
    const isNonMobile = useMediaQuery("(min-width:600px)");

    return (
        <Box mb="1rem" display={'flex'} gap="1rem" p="1rem" justifyContent={'flex-start'} alignItems={'center'} borderRadius={"6px"} border={'2px solid #3EBDE0'} >
            {isNonMobile && (
                <img src={`https://i.pravatar.cc/50?img=${index}`} style={{ borderRadius: '50%' }} />

            )}
            <Box gap="0.25rem" alignItems={'center'} justifyContent={'flex-start'}>
                <Typography fontWeight={'bold'} color={'#3EBDE0'}>{name}</Typography>
                <Typography color={'#3EBDE0'}>{email} </Typography>
            </Box>
            <Box flexBasis={'70%'} justifyContent={'flex-end'} display={'flex'}>
                <Typography color={'#3EBDE0'} fontWeight={'bold'} fontSize={isNonMobile ? '2rem' : '1rem'}>{numOfOrder} đơn hàng</Typography>
            </Box>
        </Box>
    )
}

export default RowUser