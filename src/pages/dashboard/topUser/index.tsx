import { Box, Typography, useMediaQuery } from '@mui/material'
import React, { useEffect } from 'react'
import RowUser from './rowUser'
import axios from 'axios'
import { url } from '../../../url'

type Props = {

}
type User = {
    name: string,
    email: string,
    num: number,
}

const TopUser = (props: Props) => {
    const [data, setData] = React.useState<any[]>([])
    const [dataRender, setDataRender] = React.useState<User[]>([])
    const [isLoading, setIsLoading] = React.useState(false)

    const bearerToken = localStorage.getItem('sessionId')
    const isNonMobile = useMediaQuery("(min-width:600px)");


    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`
    }
    useEffect(() => {
        setIsLoading(true)
        try {
            axios({
                method: "get",
                url: `${url}/statistics/top-customers/5`,
                headers: headers
            }).then((res) => {
                setData(res.data.data)
                setIsLoading(false)
            }).catch((error) => {
                console.log(error);
                setIsLoading(false)
            })
        } catch (error) {
            console.log(error);
            setIsLoading(false)
        }
    }, [])

    React.useEffect(() => {
        if (data) {
            let mapData = data.map((item) => {
                return {
                    name: item.attributes.name,
                    email: item.attributes.email,
                    num: item.attributes.number_of_orders
                }
            })
            setDataRender(mapData)
        }
    }, [data])
    return (
        <Box p="1rem" borderRadius={'4px'} bgcolor={'white'} >
            <Typography variant='h6' fontWeight={'bold'} color="black" pb="1rem">Khách hàng của tháng </Typography>
            {dataRender.map((item, index) => {
                return (
                    <RowUser index={index} name={item.name} email={item.email} numOfOrder={item.num} />

                )
            })}
        </Box>
    )
}

export default TopUser