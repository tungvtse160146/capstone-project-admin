import { Box, Typography, useMediaQuery } from '@mui/material'
import React, { useEffect } from 'react'
import axios from 'axios'
import { url } from '../../../url'
import RowPartner from './rowPartner'

type Props = {

}
type User = {
    name: string,
    num: number,
    avatar_url: string
}

const TopPartner = (props: Props) => {
    const [data, setData] = React.useState<any[]>([])
    const [dataRender, setDataRender] = React.useState<User[]>([])
    const [isLoading, setIsLoading] = React.useState(false)


    const bearerToken = localStorage.getItem('sessionId')

    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`
    }
    useEffect(() => {
        setIsLoading(true)
        try {
            axios({
                method: "get",
                url: `${url}/statistics/top-partners/5`,
                headers: headers
            }).then((res) => {
                setData(res.data.data)
                setIsLoading(false)
            }).catch((error) => {
                console.log(error);
                setIsLoading(false)
            })
        } catch (error) {
            console.log(error);
            setIsLoading(false)
        }
    }, [])

    React.useEffect(() => {
        if (data) {
            let mapData = data.map((item) => {
                return {
                    name: item.attributes.name,
                    num: item.attributes.number_of_orders,
                    avatar_url: item.attributes.avatar_url
                }
            })
            setDataRender(mapData)
        }
    }, [data])
    return (
        <Box p="1rem" borderRadius={'4px'} bgcolor={'white'} >
            <Typography variant='h6' fontWeight={'bold'} color="black" pb="1rem">Đối tác của tháng </Typography>
            {dataRender.map((item, index) => {
                return (
                    <RowPartner index={index} name={item.name} numOfOrder={item.num} avatar_url={item.avatar_url} />

                )
            })}
        </Box>
    )
}

export default TopPartner