import { Box, Typography, useMediaQuery } from '@mui/material'
import React from 'react'
import { image } from './image'

type Props = {
    name: string,
    numOfOrder: number,
    index: number,
    avatar_url: string
}


const RowPartner = (props: Props) => {
    const { name, numOfOrder, index, avatar_url } = props
    const isNonMobile = useMediaQuery("(min-width:600px)");

    return (
        <Box mb="1rem" display={'flex'} gap="1rem" p="1rem" justifyContent={'space-between'} alignItems={'center'} borderRadius={"6px"} border={'2px solid #3EBDE0'} >
            {isNonMobile && (
                <img src={avatar_url ? avatar_url : `${image[index]}`} style={{ borderRadius: '50%', width: 50, height: 50 }} />

            )}            <Box flexBasis={'50%'} gap="0.25rem" alignItems={'center'} justifyContent={'flex-start'}>
                <Typography fontWeight={'bold'} color={'#3EBDE0'}>{name}</Typography>
            </Box>
            <Box justifyContent={'flex-end'} display={'flex'}>
                <Typography color={'#3EBDE0'} fontWeight={'bold'} fontSize={isNonMobile ? '1.5rem' : '1rem'}>{numOfOrder} đơn hàng</Typography>
            </Box>
        </Box>
    )
}

export default RowPartner