import React, { useMemo, useState } from 'react';

import axios from 'axios';
import { url } from '../../../url'
import { Box, MenuItem, Typography } from '@mui/material';
import {
    MaterialReactTable,
    type MRT_ColumnDef,
} from 'material-react-table';
import { Tag } from 'antd';
import { notification } from 'antd'

type NotificationType = 'success' | 'error'



type Partner = {
    id: string;
    name: string;
    phone: string[],
    created_at: string,
    status: number
}



const DataTable = () => {
    const [sortColumn, setSortColumn] = useState();
    const [sortType, setSortType] = useState();
    const [data, setData] = useState<any[]>([])
    const [isLoading, setIsLoading] = useState(false);
    const [dataTable, setDataTable] = useState<Partner[]>([])




    const [api, contextHolder] = notification.useNotification()



    const openNotificationWithIcon = (type: NotificationType, error?: any, data?: any) => {

        api[type]({
            message: 'Cập nhật trạng thái nhà chành',
            description: type === `success` ? `Cập nhật thành công` : "Có lỗi xảy ra, vui lòng thao tác lại"
        })
    }





    const bearerToken = localStorage.getItem('sessionId')
    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`

    }



    const getPartnerList = async () => {
        const params = {
            page: 1,
            perPage: 100
        }
        setIsLoading(true)
        try {
            axios({

                method: 'get',
                url: `${url}/account/partners`,
                headers: headers,
                params: params
            }).then((res) => {
                console.log(res.data.data);
                setData(res.data.data)
                setIsLoading(false)

                localStorage.setItem('partnerCount', res.data.data.length)

            }).catch((error) => {
                console.log(error);
                setIsLoading(false)

            })
        } catch (error) {
            console.log(error);
            setIsLoading(false)

        }
    }

    const handleUpdatePartner = (id: string, status: number) => {
        let updateStatus = 0
        if (status === 0) {
            updateStatus = 1
        }
        console.log('currentStatus', status, 'Updatestatyus', updateStatus);


        try {
            axios({
                method: "PUT",
                headers: headers,
                url: `${url}/account/partners/${id}/status/${updateStatus}`
            }).then((res) => {
                console.log(res);
                getPartnerList()
                openNotificationWithIcon("success")
            }).catch((error) => {
                console.log(error);
                openNotificationWithIcon("error")

            })
        } catch (error) {
            console.log(error);
            openNotificationWithIcon("error")

        }
    }

    React.useEffect(() => {
        let resData = data.map((item) => {
            return {
                id: item.id,
                name: item.attributes.name,
                phone: item.attributes.phones,
                created_at: item.attributes.created_at,
                status: item.attributes.status,
            }
        })
        setDataTable(resData)
    }, [data])

    React.useEffect(() => {
        getPartnerList()
    }, [])

    const columns = useMemo<MRT_ColumnDef<Partner>[]>(
        () => [


            {
                accessorKey: 'name',
                header: 'Tên nhà xe',
                size: 150,


                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();


                    return (
                        <Box  >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue}</Typography>
                        </Box>

                    );
                },
            },

            {
                accessorKey: 'phone',

                header: 'Số điện thoại',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string[]>();

                    return (
                        <>
                            {cellValue.length > 0 ? (<Box display={'flex'} flexWrap={'nowrap'}>
                                {cellValue.map((item, index) => {
                                    return (
                                        <Typography px="0.5rem" fontFamily={'Roboto Mono, Menlo'} >{item}{index !== cellValue.length - 1 ? ', ' : ''} </Typography>
                                    )
                                })}

                            </Box>) : (
                                <Box display={'flex'} flexWrap={'nowrap'}>
                                    <Typography fontFamily={'Roboto Mono, Menlo'} >Đang cập nhật</Typography>

                                </Box>
                            )}
                        </>
                    )
                }
            },
            {
                accessorKey: 'created_at',
                header: 'Ngày tham gia',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();

                    return (
                        <Box >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue.split('T')[0]}</Typography>
                        </Box>
                    );
                },
            },
            {
                accessorKey: 'status',
                header: 'Trạng thái',
                size: 150,
                Cell: ({ cell }) => {
                    let cellString = cell.getValue<number>();

                    return (
                        <Box alignItems={'center'} justifyContent={'center'} display={'flex'} >
                            <Tag color={cellString === 1 ? 'success' : 'error'}> {cellString === 1 ? "Hoạt động" : "Ngừng hoạt động"} </Tag>
                        </Box>

                    );
                },


            },

        ],
        []
    );

    return (
        <>
            {contextHolder}

            <MaterialReactTable columns={columns} data={dataTable}
                state={{
                    isLoading: isLoading
                }}
                muiTableBodyRowProps={({ row }) => ({

                    sx: {
                        transition: 'background-color 0.3s', // Add a transition effect for smoother hover
                        '&:hover': {
                            filter: 'brightness(1.1)',
                        },
                        height: '6px',
                    },
                })}
                muiTableHeadCellProps={{
                    align: 'center'

                }}
                initialState={{
                    pagination: {
                        pageSize: 10,
                        pageIndex: 0,
                    },
                    density: 'compact',
                }}

                renderRowActionMenuItems={
                    ({ row, closeMenu }) => [
                        <MenuItem key="proceed" onClick={() => {
                            closeMenu()
                            handleUpdatePartner(row.original.id, row.original.status)
                        }}>
                            {row.original.status === 1 ? 'Vô hiệu hóa tài khoản' : 'Kích hoạt tài khoản'}
                        </MenuItem>,
                        <MenuItem onClick={() => [
                            closeMenu()
                        ]} key="cancel">
                            Hủy
                        </MenuItem>
                    ]
                }
                enableRowActions={true}
            />

        </>
    );
};

export default DataTable;
