import { Box } from '@mui/material';
import { Button, Image, Modal } from 'antd';
import React from 'react'

type Props = {
    image: any,
    currentId: string,
    setIsTopUpModalOpen: (value: any) => void,
    proceedTransaction: (id: any) => Promise<void>,
    isTopUpModalOpen: boolean,

}

const ModalTopup = ({ image, currentId, setIsTopUpModalOpen, isTopUpModalOpen, proceedTransaction }: Props) => {
    const handleSubmit = () => {
        proceedTransaction(currentId)
        setIsTopUpModalOpen(false)
    };
    return (
        <Modal
            title={"Xác nhận nạp tiền"}
            footer={false}
            onCancel={() => setIsTopUpModalOpen(false)}
            open={isTopUpModalOpen}
            width={400}
        >
            <Box mb={'1rem '}>
                <Image src={image} style={{ height: 400 }} />

            </Box>
            <Box display='flex' gap="0.5rem" justifyContent={'flex-end'}>
                <Button
                    onClick={handleSubmit}
                    type="primary"
                    className="bg-[#1677ff]"
                    htmlType="submit"
                    style={{ marginRight: "15px" }}
                >
                    Xác nhận
                </Button>

                <Button
                    type="default"
                    onClick={() => {
                        setIsTopUpModalOpen(false);
                    }}
                >
                    Hủy
                </Button>
            </Box>
        </Modal >
    )
}

export default ModalTopup