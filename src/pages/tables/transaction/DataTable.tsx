import React, { useMemo, useState } from 'react';
import {
    Checkbox,
} from 'rsuite';
import axios from 'axios';
import { url } from '../../../url'
import { Box, MenuItem, Tooltip, Typography } from '@mui/material';
import {
    MaterialReactTable,
    type MRT_ColumnDef,
} from 'material-react-table';
import { Button, Modal, Tag } from 'antd';
import { notification } from 'antd'
import ModalWithDraw from './ModalWithdraw';
import ModalTopup from './ModalTopup';
import dayjs from "dayjs";


type NotificationType = 'success' | 'error'

type Transaction = {
    id: string,
    type: string,
    partner_id: string,
    partner_name: string,
    type_action: string,
    amount: number,
    created_at: string,
    bank_account_number: string,
    bank_account_name: string,
    bank_code: string,
    is_proceeded: Boolean,
    image_url: string,
}

const DataTable = () => {
    const [data, setData] = useState<any[]>([])
    const [isLoading, setIsLoading] = useState(false);
    const [dataTable, setDataTable] = useState<Transaction[]>([])
    const [isCheckRender, setIsCheckRender] = useState<string>('is_not_proceeded')
    const [totalLength, setTotalLength] = useState(0)
    const [isShowModalWithdraw, setIsShowModalWithdraw] = useState(false)
    const [isShowModalTopup, setIsShowModalTopup] = useState(false)
    const [currentId, setCurrentId] = useState('')
    const [currentImage, setCurrentImage] = useState('')
    const [isVisibleModalInformation, setIsVisibleModalInformation] = useState(false)


    const bearerToken = localStorage.getItem('sessionId')
    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`
    }
    const [api, contextHolder] = notification.useNotification()

    const openNotificationWithIcon = (type: NotificationType, error?: any, data?: any) => {
        api[type]({
            message: 'Thực hiện giao dịch',
            description: type === `success` ? `Giao dịch đã hoàn tất` : "Có lỗi xảy ra, vui lòng thao tác lại"
        })
    }

    const convertType = (s: string) => {
        if (s === 'is_proceeded') {
            return true
        }
        else if (s === 'is_not_proceeded') {
            return false
        }
    }

    const getTransactionList = async () => {
        setIsLoading(true)
        const params = {
            page: 1,
            perPage: totalLength
        }
        try {
            axios({

                method: 'get',
                url: `${url}/transaction-requests`,
                headers: headers,
                params: params
            }).then((res) => {
                setTotalLength(res.data.meta.total)

                console.log(res.data.data, res);
                setData(res.data.data)
                setIsLoading(false)


            }).catch((error) => {
                console.log(error);
                setIsLoading(false)

            })
        } catch (error) {
            console.log(error);
            setIsLoading(false)

        }
    }

    const proceedTransactionWithdraw = async (id: string, img: any) => {
        try {
            axios({
                method: "post",
                headers: headers,
                url: `${url}/transaction-requests/withdraw/${id}`,
                data: { image: img }
            }).then((res) => {
                console.log(res);
                getTransactionList()
                openNotificationWithIcon("success")
            }).catch((error) => {
                console.log(error);
                openNotificationWithIcon("error")
            })
        } catch (error) {
            console.log(error);
            openNotificationWithIcon("error")
        }
    }

    const proceedTransactionTopUp = async (id: string) => {
        try {
            axios({
                method: "post",
                headers: headers,
                url: `${url}/transaction-requests/top-up/${id}`,
            }).then((res) => {
                console.log(res);
                getTransactionList()
                openNotificationWithIcon("success")
            }).catch((error) => {
                console.log(error);
                openNotificationWithIcon("error")
            })
        } catch (error) {
            console.log(error);
            openNotificationWithIcon("error")
        }
    }

    const handleWithdrawPopup = () => {
        setIsShowModalWithdraw(true)
    }





    React.useEffect(() => {
        getTransactionList()
        console.log(totalLength);

    }, [totalLength])

    React.useEffect(() => {
        let resData = data.map((item) => {
            return {
                id: item.id,
                type: item.type,
                partner_id: item.attributes.partner_id,
                partner_name: item.attributes.partner_name,
                type_action: item.attributes.type,
                amount: item.attributes.amount,
                created_at: item.attributes.created_at,
                bank_account_number: item.attributes.bank_account_number,
                bank_account_name: item.attributes.bank_account_name,
                bank_code: item.attributes.bank_code,
                is_proceeded: item.attributes.is_proceeded,
                image_url: item.attributes.image_url
            }
        })
        setDataTable(resData)
    }, [data])

    const columns = useMemo<MRT_ColumnDef<Transaction>[]>(
        () => [
            {
                accessorKey: 'type_action',
                header: 'Loại giao dịch',
                size: 100,


                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();
                    if (cellValue.length > 20) {
                        const firstThree = cellValue.slice(0, 15);
                        cellValue = `${firstThree}...`;
                    }
                    return (
                        <Tooltip title={cell.getValue<string>()}>
                            <Box  >
                                <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'left'} color={'black'} fontSize={'14px'} sx={{
                                }} >{cellValue === 'Topup' ? 'Nạp tiền' : 'Rút tiền'}</Typography>
                            </Box>
                        </Tooltip>
                    );
                },
            },

            {
                accessorKey: 'amount',

                header: 'Số tiền',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<number>().toLocaleString('en-US');
                    if (cellValue.length > 20) {
                        const firstThree = cellValue.slice(0, 15);
                        cellValue = `${firstThree}...`;
                    }
                    return (
                        <Tooltip title={`${cellValue} VND`}>
                            <Box  >
                                <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'left'} color={'black'} fontSize={'14px'} sx={{
                                }} >{cellValue} VND</Typography>
                            </Box>
                        </Tooltip>
                    );
                },
            },
            {
                accessorKey: 'bank_account_number',
                header: 'Số tài khoản',
                size: 100,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();
                    if (cellValue.length > 20) {
                        const firstThree = cellValue.slice(0, 15);
                        cellValue = `${firstThree}...`;
                    }
                    return (
                        <Tooltip title={cell.getValue<string>()}>
                            <Box  >
                                <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'left'} color={'black'} fontSize={'14px'} sx={{
                                }} >{cellValue}</Typography>
                            </Box>
                        </Tooltip>
                    );
                },
            },
            {
                accessorKey: 'bank_account_name',
                header: 'Tên tài khoản',
                size: 100,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();
                    if (cellValue.length > 20) {
                        const firstThree = cellValue.slice(0, 15);
                        cellValue = `${firstThree}...`;
                    }
                    return (
                        <Tooltip title={cell.getValue<string>()}>
                            <Box  >
                                <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'left'} color={'black'} fontSize={'14px'} sx={{
                                }} >{cellValue}</Typography>
                            </Box>
                        </Tooltip>
                    );
                },


            },
            {
                accessorKey: 'bank_code',
                header: 'Ngân hàng',
                size: 100,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();
                    if (cellValue.length > 20) {
                        const firstThree = cellValue.slice(0, 15);
                        cellValue = `${firstThree}...`;
                    }
                    return (
                        <Tooltip title={cell.getValue<string>()}>
                            <Box  >
                                <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'left'} color={'black'} fontSize={'14px'} sx={{
                                }} >{cellValue}</Typography>
                            </Box>
                        </Tooltip>
                    );
                },


            },
            {
                accessorKey: 'created_at',
                header: 'Thời gian',
                size: 100,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();

                    return (
                        <Tooltip title={cell.getValue<string>()}>
                            <Box  >
                                <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'left'} color={'black'} fontSize={'14px'} sx={{
                                }} >{dayjs(cellValue).format("HH:mm:ss ngày DD/MM/YYYY")}</Typography>
                            </Box>
                        </Tooltip>);
                },


            },
            {
                accessorKey: 'is_proceeded',
                header: 'Trạng thái',
                size: 100,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<Boolean>();

                    return (
                        <Tag color={cellValue === true ? "success" : 'processing'} > {cellValue === true ? "Đã thực hiện" : "Chờ duyệt"} </Tag>
                    );
                },


            },

        ],
        [isCheckRender]
    );





    return (
        <>
            {contextHolder}

            <MaterialReactTable columns={columns} data={dataTable.filter((item) => item.is_proceeded === convertType(isCheckRender))
            }
                state={{
                    isLoading: isLoading
                }}
                muiTableBodyRowProps={({ row }) => ({

                    sx: {
                        transition: 'background-color 0.3s', // Add a transition effect for smoother hover
                        '&:hover': {
                            filter: 'brightness(1.1)',
                        },
                        height: '6px',
                    },
                })}
                muiTableHeadCellProps={{

                }}
                enableRowActions={isCheckRender === "is_not_proceeded"}


                initialState={{
                    pagination: {
                        pageSize: 10,
                        pageIndex: 0,
                    },
                    density: 'compact',
                }}
                renderRowActionMenuItems={
                    ({ row, closeMenu }) => [
                        <MenuItem key="proceed" onClick={() => {
                            if (isCheckRender === "is_not_proceeded") {
                                setCurrentId(row.original.id)
                                console.log(row.original.type_action);

                                if (row.original.type_action === 'Withdraw') {
                                    handleWithdrawPopup()
                                }
                                else if (row.original.type_action === 'Topup') {
                                    setIsShowModalTopup(true)
                                    setCurrentImage(row.original.image_url)
                                }
                            }
                            else {
                                setIsVisibleModalInformation(true)
                                setCurrentImage(row.original.image_url)
                            }
                            closeMenu()
                        }}>
                            {isCheckRender === 'is_proceeded' ? 'Thông tin giao dịch' : 'Xác nhận giao dịch'}
                        </MenuItem>,
                        <MenuItem onClick={() => [
                            closeMenu()
                        ]} key="cancel">
                            Hủy
                        </MenuItem>
                    ]
                }
                renderTopToolbarCustomActions={() => {
                    return (
                        <>
                            <Box display="flex" justifyContent={"center"} alignItems={"center"} gap="0.5rem">
                                <Checkbox checked={isCheckRender === 'is_not_proceeded'} onChange={() => {
                                    setIsCheckRender('is_not_proceeded')
                                }} />
                                <Box mr="2rem" display={'flex'} gap="0.5rem" justifyContent={'center'} alignItems={'center'}>
                                    <Tag color='processing'>Đang chờ duyệt</Tag>
                                </Box>

                                <Checkbox checked={isCheckRender === 'is_proceeded'} onChange={() => {
                                    setIsCheckRender('is_proceeded')
                                }} />
                                <Box mr="2rem" display={'flex'} gap="0.5rem" justifyContent={'center'} alignItems={'center'}>
                                    <Tag color='success'>Đã thực hiện</Tag>
                                </Box>
                            </Box>
                        </>
                    )
                }}

            />

            <ModalWithDraw proceedTransaction={proceedTransactionWithdraw} isWithdrawModalOpen={isShowModalWithdraw} setIsWithdrawModalOpen={setIsShowModalWithdraw} currentId={currentId} />
            <ModalTopup proceedTransaction={proceedTransactionTopUp} isTopUpModalOpen={isShowModalTopup} setIsTopUpModalOpen={setIsShowModalTopup} currentId={currentId} image={currentImage} />
            <Modal
                title={"Thông tin giao dịch"}
                footer={false}
                onCancel={() => setIsVisibleModalInformation(false)}
                open={isVisibleModalInformation}
                width={400}
            >
                <Box mb={'1rem '}>
                    <img src={currentImage} style={{ width: 300 }} />

                </Box>
                <Box display='flex' gap="0.5rem" justifyContent={'flex-end'}>
                    <Button
                        type="primary"
                        className="bg-[#1677ff]"
                        htmlType="submit"
                        style={{ marginRight: "15px" }}
                        onClick={() => setIsVisibleModalInformation(false)}
                    >
                        Đóng
                    </Button>


                </Box>
            </Modal >
        </>
    );
};

export default DataTable;
