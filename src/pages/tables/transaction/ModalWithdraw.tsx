import React, { useEffect, useState } from "react";
import { Modal, Row, Col, Select, Button, Form, Input, Spin, UploadFile, Upload } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { RcFile } from "antd/es/upload";

interface Props {
    isWithdrawModalOpen: boolean;
    setIsWithdrawModalOpen: (value?: any) => void;
    currentId: string;
    proceedTransaction: (id: any, img: any) => Promise<void>
}

const ModalWithDraw = ({ isWithdrawModalOpen, setIsWithdrawModalOpen, currentId, proceedTransaction }: Props) => {
    const [fileList, setFileList] = useState<UploadFile[]>([]);
    const [loading, setLoading] = React.useState(false);
    const [imageUrl, setImageUrl] = React.useState(null);

    const handleSubmit = (v: any) => {
        // let tmp = { amount: parseInt(v.amount), image: v.image[0].thumbUrl };
        // // topup(tmp);
        setIsWithdrawModalOpen(false);
        console.log(imageUrl);


        proceedTransaction(currentId, imageUrl)

    };



    function isFileImage(file: RcFile) {
        return file["type"].split("/")[0] === "image";
    }
    const normFile = (e: { fileList: any }) => {
        if (Array.isArray(e)) {
            return e;
        }
        return e?.fileList;
    };

    const getBase64 = (img: any, callback: any) => {
        const reader = new FileReader();
        console.log(typeof img);

        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    };
    const handleChange = (info: any) => {

        if (!info.file) {
            return;
        }
        else {
            getBase64(info.fileList[0].originFileObj, (imageUrl: any) => {
                setLoading(false);
                setImageUrl(imageUrl);
                console.log(imageUrl);

            });
        }
    };

    const [form] = Form.useForm();

    return (
        <Modal
            title={"Xác nhận rút tiền"}
            footer={false}
            onCancel={() => setIsWithdrawModalOpen(false)}
            open={isWithdrawModalOpen}
        >
            <Form onFinish={handleSubmit} style={{ width: '100%' }} form={form} >
                {/* TÊN */}


                <Row>
                    <Form.Item
                        name="image"
                        label="Ảnh"
                        valuePropName="fileList"
                        getValueFromEvent={normFile}
                        rules={[{ required: true, message: "Vui lòng thêm ảnh." }]}
                    >
                        <Upload
                            name="image"
                            listType="picture"
                            maxCount={1}
                            onChange={handleChange}
                            beforeUpload={(file) => {
                                const isAllowedType = isFileImage(file);
                                if (!isAllowedType) {
                                    setFileList([...fileList]);
                                    // toast.error("Files must be image!");
                                    return false;
                                }
                                setFileList([...fileList, file]);
                                return false;
                            }}
                            accept="image/*"
                        >
                            <Button icon={<UploadOutlined />}>Thêm ảnh</Button>
                        </Upload>

                    </Form.Item>
                </Row>




                <div
                    className="mt-5"
                    style={{
                        width: "100%",
                        display: "flex",
                        justifyContent: "right",
                        height: "35px",
                    }}
                >
                    <Form.Item>

                        <Button
                            type="primary"
                            className="bg-[#1677ff]"
                            htmlType="submit"
                            style={{ marginRight: "15px" }}
                        >
                            Xác nhận
                        </Button>

                    </Form.Item>
                    <Button
                        type="default"
                        onClick={() => {
                            setIsWithdrawModalOpen(false);
                            form.resetFields();
                        }}
                    >
                        Hủy
                    </Button>
                </div>
            </Form>
        </Modal>
    );
};

export default ModalWithDraw;