import React, { useEffect, useState } from "react";
import axios from "../../../config/apiConfig";
import { Pagination, Table, Tag } from "antd";
import { ColumnsType } from "antd/es/table";
import dayjs from "dayjs";
import { Box } from "@mui/material";

type Payment = {
  payment_method: string;
  value: number;
  order_code: string;
  vnpay_transaction_code: string | null;
  created_at: string;
  customer_id: number;
  customer_name: string;
  revenue: number;
  start_partner_revenue: number;
  end_partner_revenue: number;
};

const DataTable = () => {
  const bearerToken = localStorage.getItem("sessionId");
  const [data, setData] = useState<any>([]);
  const [meta, setMeta] = useState<any>();
  const [page, setPage] = useState(1);
  const payments = data.map((item: { id: any; attributes: Payment }) => ({
    id: item.id,
    order_code: item.attributes.order_code,
    payment_method: item.attributes.payment_method,
    value: item.attributes.value,
    vnpay_transaction_code: item.attributes.vnpay_transaction_code,
    created_at: item.attributes.created_at,
    customer_name: item.attributes.customer_name,
    customer_id: item.attributes.customer_id,
    revenue: item.attributes.revenue,
    start_partner_revenue: item.attributes.start_partner_revenue,
    end_partner_revenue: item.attributes.end_partner_revenue
  }));
  console.log(payments);
  useEffect(() => {
    axios
      .get(`/balance/payments?page=${page}&per_page=10`)
      .then((res) => {
        console.log(res.data);
        setData(res.data.data);
        setMeta(res.data.meta);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [page]);
  const columns: ColumnsType<Payment> = [
    {
      title: "#",
      key: "index",
      width: 50,
      align: "center",
      render: (value, item, index) => (page - 1) * 10 + index + 1,
    },
    {
      title: "Giao dịch",
      key: "order_code",
      dataIndex: ["order_code", "vnpay_transaction_code"],
      fixed: "left",
      width: 230,
      render: (text, record) => (
        <>
          <p>
            <b>
              Đơn hàng{" "}
              <a
                href={`/user/order/${record.order_code}`}
                target="_blank"
                className="uppercase"
                rel="noreferrer"
              >
                #{record.order_code}
              </a>
            </b>
          </p>
          {record.vnpay_transaction_code ? (
            <p>
              Mã VNPay: <b>{record.vnpay_transaction_code}</b>
            </p>
          ) : null}
        </>
      ),
    },
    {
      title: "Giá trị đơn hàng",
      key: "value",
      dataIndex: "value",
      sorter: (a, b) => a.value - b.value,
      width: 150,
      render: (text) => <b>{new Intl.NumberFormat("en-Us").format(text)}đ</b>,
    },
    {
      title: "Doanh thu chành 1",
      key: "start_partner_revenue",
      dataIndex: "start_partner_revenue",
      sorter: (a, b) => a.value - b.value,
      width: 150,
      render: (text) => <b>{new Intl.NumberFormat("en-Us").format(text)}đ</b>,
    },
    {
      title: "Doanh thu chành 2",
      key: "end_partner_revenue",
      dataIndex: "end_partner_revenue",
      sorter: (a, b) => a.value - b.value,
      width: 150,
      render: (text) => <b>{new Intl.NumberFormat("en-Us").format(text)}đ</b>,
    },
    {
      title: "Doanh thu hệ thống",
      key: "revenue",
      dataIndex: "revenue",
      sorter: (a, b) => a.value - b.value,
      width: 150,
      render: (text) => <b>{new Intl.NumberFormat("en-Us").format(text)}đ</b>,
    },
    {
      title: "Khách hàng",
      key: "customer_name",
      dataIndex: "customer_name",
      sorter: (a, b) => a.customer_name.localeCompare(b.customer_name),
      render: (text) => <b> {text}</b>,
    },
    {
      title: "Thời gian",
      key: "created_at",
      dataIndex: "created_at",
      sorter: (a, b) => a.created_at.localeCompare(b.created_at),
      render: (text) => <b>{dayjs(text).format("HH:mm:ss ngày DD/MM/YYYY")}</b>,
    },
    {
      title: "Phương thức thanh toán",
      key: "payment_method",
      dataIndex: "payment_method",
      render: (text) => (
        <Tag color={`${text === "Cash" ? "cyan" : "red"}`}>{text}</Tag>
      ),
    },
  ];
  const handleNavigation = (page: number, pageSize: number) => {
    setPage(page);
  };
  return (
    <Box bgcolor={"white"}>
      <Table
        size="middle"
        dataSource={payments}
        columns={columns}
        pagination={false}
        scroll={{ x: 800 }}
      />
      <Box marginTop={"10px"}>

        <Pagination
          current={meta?.current_page}
          // pageSize={meta?.per_page}
          total={meta?.total}
          showSizeChanger={false}
          onChange={(page, pageSize) => handleNavigation(page, pageSize)}
        />
      </Box>
    </Box>
  );
};

export default DataTable;
