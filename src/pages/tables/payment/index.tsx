import React from 'react';
import { Breadcrumb, Panel } from 'rsuite';
import DataTable from './DataTable';

const PaymentTable = () => {
    return (
        <Panel
            header={
                <>
                    <h3 className="title">Thanh toán khách hàng</h3>
                    <Breadcrumb>
                        <Breadcrumb.Item href="/">Trang chủ</Breadcrumb.Item>
                        <Breadcrumb.Item>Bảng</Breadcrumb.Item>
                        <Breadcrumb.Item active>Thanh toán khách hàng</Breadcrumb.Item>
                    </Breadcrumb>
                </>
            }
        >
            <DataTable />

        </Panel>
    );
};

export default PaymentTable;
