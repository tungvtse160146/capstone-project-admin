import React, { useMemo, useState } from 'react';

import axios from 'axios';
import { url } from '../../../url'
import { Box, MenuItem, Typography } from '@mui/material';
import {
    MaterialReactTable,
    type MRT_ColumnDef,
} from 'material-react-table';
import { Tag } from 'antd';




type Payment = {
    id: string;
    type: string,
    amount: number,
    description: string,
    created_at: string,
    partner_name: string
}



const DataTable = () => {
    const [data, setData] = useState<any[]>([])
    const [isLoading, setIsLoading] = useState(false);
    const [dataTable, setDataTable] = useState<Payment[]>([])


    const bearerToken = localStorage.getItem('sessionId')
    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`

    }

    const convertType = (s: string, amount?: number) => {
        if (s === "Revenue") {
            return "Phí vận chuyển"
        }
        else if (s === "CollectionOnBehalf") {
            return "Phí thu hộ"
        }
        else if (s === "Cash") {
            if (amount && amount > 0) {
                return "Giao dịch nạp tiền "
            }
            else {
                return "Giao dịch rút tiền"
            }
        }
    }


    const getPaymentList = async () => {
        const params = {
            page: 1,
            perPage: 100
        }
        setIsLoading(true)
        try {
            axios({

                method: 'get',
                url: `${url}/balance/transactions`,
                headers: headers,
                params: params
            }).then((res) => {
                console.log(res.data.data);
                setData(res.data.data)
                setIsLoading(false)

                localStorage.setItem('partnerCount', res.data.data.length)

            }).catch((error) => {
                console.log(error);
                setIsLoading(false)

            })
        } catch (error) {
            console.log(error);
            setIsLoading(false)

        }
    }

    React.useEffect(() => {
        console.log(dataTable);

    }, [dataTable])


    React.useEffect(() => {
        let resData: Payment[] = data.map((item) => {
            return {
                id: item.id,
                type: item.attributes.type,
                amount: item.attributes.amount,
                created_at: item.attributes.created_at.split('T')[0],
                description: item.attributes.description,
                partner_name: item.attributes.partner_name
            }
        })
        setDataTable(resData)
    }, [data])

    React.useEffect(() => {
        getPaymentList()
    }, [])

    const columns = useMemo<MRT_ColumnDef<Payment>[]>(
        () => [


            {
                accessorKey: 'id',
                header: 'Id',
                size: 150,


                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();


                    return (
                        <Box textAlign={'center'} >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue}</Typography>
                        </Box>

                    );
                },
            },

            {
                accessorKey: 'type',

                header: 'Loại giao dịch',
                size: 150,
                Cell: ({ cell, row }) => {
                    let cellValue = cell.getValue<string>();
                    let amount = row.original.amount

                    return (
                        <Typography textAlign={'center'} >
                            {convertType(cellValue, amount)}
                        </Typography>
                    )
                }
            },
            {
                accessorKey: 'amount',

                header: 'Số tiền',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<number>().toLocaleString('en-US');

                    return (
                        <Typography color={cell.getValue<number>() > 0 ? 'green' : 'red'} textAlign={'center'} >
                            {cellValue} VNĐ
                        </Typography>
                    )
                }
            },



        ],
        []
    );

    return (
        <>

            <MaterialReactTable columns={columns} data={dataTable}
                state={{
                    isLoading: isLoading
                }}
                muiTableBodyRowProps={({ row }) => ({

                    sx: {
                        transition: 'background-color 0.3s', // Add a transition effect for smoother hover
                        '&:hover': {
                            filter: 'brightness(1.1)',
                        },
                        height: '6px',
                    },
                })}
                muiTableHeadCellProps={{
                    align: 'center'

                }}
                initialState={{
                    pagination: {
                        pageSize: 10,
                        pageIndex: 0,
                    },
                    density: 'compact',
                }}


            />

        </>
    );
};

export default DataTable;
