import React, { useEffect, useMemo, useState } from 'react';
import {
    Input,
    InputGroup,
    Table,
    Button,
    DOMHelper,
    Progress,
    Checkbox,
    Stack,
    SelectPicker
} from 'rsuite';
import SearchIcon from '@rsuite/icons/Search';
import MoreIcon from '@rsuite/icons/legacy/More';
import axios from 'axios';
import { url } from '../../../url'
import { Box, MenuItem, Typography } from '@mui/material';
import { Tag } from "antd"
import {
    MaterialReactTable,
    type MRT_ColumnDef,
} from 'material-react-table';
import { useGetUserQuery } from '../../../services/apiSlice';



type User = {
    id: string
    email: string,
    name: string,
    phone: string,
    status: 1 | 0,

}


const DataTable = () => {

    const [dataTable, setDataTable] = useState<User[]>([])


    const { data, error, isLoading, isSuccess } = useGetUserQuery({
        page: 1,
        perPage: 100,
    });
    console.log(error);


    React.useEffect(() => {
        if (data) {
            let resData = data.data.map((item: any) => {
                return {
                    id: item.id,
                    email: item.attributes.email,
                    name: item.attributes.name,
                    phone: item.attributes.phone,
                    status: item.attributes.status
                }
            })
            setDataTable(resData)
        }

    }, [data])

    React.useEffect(() => {
        console.log(dataTable);

    }, [dataTable])

    const columns = useMemo<MRT_ColumnDef<User>[]>(
        () => [

            {
                accessorKey: 'email',
                header: 'Email',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();

                    return (
                        <Box  >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue}</Typography>
                        </Box>
                    );
                },
            },
            {
                accessorKey: 'name',
                header: 'Tên khách hàng',
                size: 150,


                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();


                    return (
                        <Box  >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue}</Typography>
                        </Box>

                    );
                },
            },

            {
                accessorKey: 'phone',

                header: 'Số điện thoại',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();

                    return (
                        <Box  >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue}</Typography>
                        </Box>
                    )
                }
            },
            {
                accessorKey: 'status',
                header: 'Trạng thái',
                size: 150,
                Cell: ({ cell }) => {
                    let cellString = cell.getValue<number>();

                    return (
                        <Box alignItems={'center'} justifyContent={'center'} display={'flex'} >
                            <Tag color={cellString === 1 ? 'success' : 'error'}> {cellString === 1 ? "Hoạt động" : "Ngừng hoạt động"} </Tag>
                        </Box>

                    );
                },


            },

        ],
        []
    );


    return (
        <>

            {!error && (
                <MaterialReactTable columns={columns} data={dataTable}
                    state={{
                        isLoading: isLoading
                    }}
                    muiTableBodyRowProps={({ row }) => ({

                        sx: {
                            transition: 'background-color 0.3s', // Add a transition effect for smoother hover
                            '&:hover': {
                                filter: 'brightness(1.1)',
                            },
                            height: '6px',
                        },
                    })}
                    muiTableHeadCellProps={{
                        align: 'center'

                    }}
                    initialState={{
                        pagination: {
                            pageSize: 10,
                            pageIndex: 0,
                        },
                        density: 'compact',
                    }}
                // renderRowActionMenuItems={
                //     ({ row, closeMenu }) => [
                //         <MenuItem key="proceed" onClick={() => {
                //             closeMenu()
                //         }}>
                //             Vô hiệu hóa tài khoản
                //         </MenuItem>,
                //         <MenuItem onClick={() => [
                //             closeMenu()
                //         ]} key="cancel">
                //             Hủy
                //         </MenuItem>
                //     ]
                // }
                // enableRowActions={true}
                />
            )}




        </>
    );
};

export default DataTable;
