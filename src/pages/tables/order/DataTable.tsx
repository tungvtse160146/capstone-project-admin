import React, { useEffect, useState } from 'react';
import {
    InputGroup,
    Table,
    DOMHelper,
    Progress,
    Checkbox,
    Stack,
    SelectPicker, Modal,
    Placeholder,
    Whisper,
    IconButton,
    Popover,
    Dropdown,
    Loader,
    Pagination,
    Input
} from 'rsuite';
import SearchIcon from '@rsuite/icons/Search';
import MoreIcon from '@rsuite/icons/legacy/More';
// import DrawerView from './DrawerView';
// import { NameCell, ImageCell, CheckCell, ActionCell, StatusCell } from '../Cells';
import { Box, Typography } from '@mui/material';
import axios from 'axios';
import { url } from '../../../url'
import OrderComponent from './Order';
import { Empty, Button, Select } from 'antd';




type Props = {
}

type Permissions = {
    achieved_at: string,
    can_be_achieved: Boolean,
    permission: number,
    permission_number: number
}

type Checkpoints = {
    id: string,
    name: string,
    address: string,
    type: string,
    driver: string,
    vehicle: string,
    permissions: Permissions[]
}

type Order = {
    id: number;
    code: string;
    checkpoints: Checkpoints[],
    height: number,
    length: number,
    weight: number,
    width: number,
    note: string,
    package_types: number[],
    package_value: number,
    receiver_name: string,
    receiver_phone: string,
    sender_name: string,
    sender_phone: string,
    is_confirmed: Boolean
}

export type Vehicle = {
    image_url: string,
    partner_name: string,
    plate_number: string,
    type: string,
    id: string
}

export type Driver = {
    id: string,
    avatar_url: string,
    name: string,
    partner_name: string,
    phone: string
}

const DataTable = (props: Props) => {
    const [isLoading, setIsLoading] = useState(false)
    const [dataArray, setDataArray] = useState([]);
    const [dataRender, setDataRender] = useState<any[]>([]);
    const [isCheck, setIsCheck] = useState<string>("straight")
    const [activePage, setActivePage] = useState(1)
    const [activePageDone, setActivePageDone] = useState(1)
    const [currentLength, setCurrentLength] = useState(0)
    const [searchOrder, setSearchOrder] = useState<string>('')
    const [typeSearch, setTypeSearch] = useState<string>('current_plate_number')
    const [isDone, setIsDone] = useState<string>('not_done')
    const [isComingToHub, setIsComingToHub] = useState(false)
    const [vehiclesComing, setVehiclesComing] = useState<string[]>([])

    const handleSearchOrder = (s: string) => {
        getOrderList(activePage, isCheck, s, typeSearch, isComingToHub)
    }

    const role = localStorage.getItem('role')



    React.useEffect(() => {
        const newData = dataArray.map((item: any, i) => {
            return {
                id: item.id,
                code: item.attributes.code,
                sender_name: item.attributes.sender_name,
                sender_phone: item.attributes.sender_phone,
                receiver_name: item.attributes.receiver_name,
                receiver_phone: item.attributes.receiver_phone,
                weight: item.attributes.weight,
                height: item.attributes.height,
                length: item.attributes.length,
                width: item.attributes.width,
                package_types: item.attributes.package_types,
                package_value: item.attributes.package_value,
                note: item.attributes.note,
                checkpoints: item.attributes.checkpoints,
                is_confirmed: item.attributes.is_confirmed,
                end_partner_id: item.attributes.end_partner_id,
                revenue: item.attributes.revenue,
                delivery_price: item.attributes.delivery_price,
                first_partner_revenue: item.attributes.first_partner_revenue,
                second_partner_revenue: item.attributes.second_partner_revenue,
                package_image_url: item.attributes.package_image_url,
                delivered_image_url: item.attributes.delivered_image_url
            }
        })
        setDataRender(newData)
    }, [dataArray])

    const bearerToken = localStorage.getItem('sessionId')

    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`
    }


    const getOrderList = async (i: number, isCheck: string, input: string, type: string, isComingToHub: boolean) => {
        const params = {
            page: i,
            perPage: 5
        }

        let isPassThroughHub = false

        if (isCheck == 'throughHub' || role === 'staff') {
            isPassThroughHub = true
        }

        setIsLoading(true)
        try {
            axios({
                method: 'get',
                url: `${url}/orders?filter[is_pass_through_hub]=${isPassThroughHub}&filter[${type}]=${input}${isComingToHub ? '&filter[status]=2&filter[is_at_start_checkpoint]=true' : ''}`,
                headers: headers,
                params: params
            }).then((res) => {
                setDataArray(res.data.data)
                setIsLoading(false)
                setCurrentLength(res.data.meta.total)
            }).catch((error) => {
                console.log(error);
                setIsLoading(false)
            })
        } catch (error) {
            console.log(error);
            setIsLoading(false)
        }


    }

    const getOrderDoneList = (i: number) => {
        const params = {
            page: i,
            perPage: 5
        }

        setIsLoading(true)
        try {
            axios({
                method: 'get',
                url: `${url}/orders/done`,
                headers: headers,
                params: params
            }).then((res) => {
                setDataArray(res.data.data)
                setIsLoading(false)
                setCurrentLength(res.data.meta.total)
            }).catch((error) => {
                console.log(error);
                setIsLoading(false)
            })
        } catch (error) {
            console.log(error);
            setIsLoading(false)
        }
    }

    const getVehiclesComing = async () => {
        try {
            axios({
                method: 'get',
                headers: headers,
                url: `${url}/vehicles/in-coming`,
                params: {
                    per_page: 1000
                }
            }).then((res) => {
                setVehiclesComing(res.data.data.map((item: any) => item.attributes.plate_number))
                console.log(res, 'res');

            }).catch((error) => { })
        } catch (error) {

        }
    }

    React.useEffect(() => {
        getVehiclesComing()
    }, [])
    React.useEffect(() => {
        if (isDone === 'not_done') {
            getOrderList(activePage, isCheck, searchOrder, typeSearch, isComingToHub)

        }
        else {
            getOrderDoneList(activePageDone)
        }

    }, [activePage, isCheck, role, isDone, activePageDone, isComingToHub])


    React.useEffect(() => {
        setActivePage(1)
        setActivePageDone(1)
    }, [isDone])



    return (
        <>
            {!isLoading ? (<>
                <Box mb="1rem" display={'flex'} justifyContent={'space-between'}>

                    <Select options={[
                        {
                            value: 'not_done',
                            label: 'Đơn chưa hoàn tất'
                        },
                        {
                            value: 'done',
                            label: 'Đơn đã hoàn tất',
                        },


                    ]}
                        value={isDone}
                        onSelect={setIsDone}
                        style={{
                            width: 200
                        }}
                    />

                    {isDone === 'not_done' && role === 'staff' && (
                        <Box display={'flex'} gap="0.5rem">
                            <Typography fontWeight={'bold'}>Các xe đang đến hub: </Typography>
                            {vehiclesComing.map((item) => {
                                return (
                                    <Typography fontWeight={'bold'}>{item}</Typography>
                                )
                            })}
                        </Box>

                    )}
                </Box>
                {
                    isDone === 'not_done' && (
                        <Box display={'flex'} gap="0.5rem" justifyContent={'center'} alignItems={'center'} mb="1rem">

                            <Select options={[
                                {
                                    value: 'code',
                                    label: 'Mã đơn hàng',
                                },
                                {
                                    value: 'customer_name',
                                    label: 'Tên khách hàng'
                                },
                                {
                                    value: 'customer_phone',
                                    label: 'Số điện thoại'
                                },
                                {
                                    value: 'current_plate_number',
                                    label: 'Biển số xe'
                                }
                            ]}
                                value={typeSearch}
                                onSelect={setTypeSearch}
                                style={{
                                    width: 200
                                }}
                            />
                            <Input onChange={setSearchOrder} width={200} value={searchOrder} />
                            <Button onClick={() => { handleSearchOrder(searchOrder) }} type='primary'> Tìm kiếm</Button>
                        </Box>
                    )
                }
                {role === 'staff' && isDone === 'not_done' && (
                    <Checkbox checked={isComingToHub} onChange={() => { setIsComingToHub(!isComingToHub) }}>Đơn đang đến hub </Checkbox>
                )}
                {role === 'admin' && isDone === 'not_done' ? (<>
                    <Box display={'flex'} gap="1rem">
                        <Checkbox checked={isCheck === "straight"} onChange={() => {
                            setIsCheck("straight")
                        }} >Đơn đi thẳng</Checkbox>
                        <Checkbox checked={isCheck === "throughHub"} onChange={() => {
                            setIsCheck("throughHub")
                        }}>Đơn qua hub</Checkbox>
                    </Box>
                    {isCheck === "throughHub" ? (<>
                        <Typography pt='2rem' pb="2rem" variant='h5' color="black" fontWeight={'bold'}> Đơn qua hub</Typography>
                    </>) : (<>
                        <Typography pt='2rem' pb="2rem" variant='h5' color="black" fontWeight={'bold'}> Đơn đi thẳng</Typography>
                    </>)}

                    {dataRender.length ? dataRender.map((item) => {
                        return (
                            <Box mb="2rem">
                                <OrderComponent {...item} role={role} onChange={() => { getOrderList(activePage, isCheck, searchOrder, typeSearch, isComingToHub) }} />
                            </Box>
                        )
                    }) : (<Empty description="Hiện tại không có đơn hàng" />)}
                </>) : (<>{dataRender.length ? dataRender.map((item) => {
                    return (
                        <Box mb="2rem">
                            <OrderComponent {...item} role={role} onChange={() => { getOrderList(activePage, isCheck, searchOrder, typeSearch, isComingToHub) }} />
                        </Box>
                    )
                }) : (<Empty description="Hiện tại không có đơn hàng" />)}
                </>)}
                {dataRender.length ? (<Box justifyContent={'center'}  >
                    {isDone === 'not_done' ? (
                        <Pagination prev last next first size="md" total={currentLength} limit={5} activePage={activePage} onChangePage={setActivePage} />
                    ) : (
                        <Pagination prev last next first size="md" total={currentLength} limit={5} activePage={activePageDone} onChangePage={setActivePageDone} />
                    )
                    }

                </Box>) : <></>}

            </>) : (<Loader speed="slow" center content="Đang tải dữ liệu" />)}
        </>
    );
};

export default DataTable;
