import React from 'react';
import { Breadcrumb, Panel } from 'rsuite';
import { Box, Divider, Typography } from '@mui/material';
import DataTable from './DataTable';
import { useNavigate } from 'react-router-dom';



const OrderTable = () => {
    const navigate = useNavigate();
    const token = localStorage.getItem('sessionId')
    if (token === null) {
        navigate("login")
    }
    return (
        <Panel
            header={
                <>
                    <h3 className="title">Danh sách đơn hàng</h3>
                    <Breadcrumb >
                        <Breadcrumb.Item style={{ fontSize: '16px' }} href="/">Trang chủ</Breadcrumb.Item>
                        <Breadcrumb.Item style={{ fontSize: '16px' }}>Bảng</Breadcrumb.Item>
                        <Breadcrumb.Item style={{ fontSize: '16px' }} active>Danh sách đơn hàng</Breadcrumb.Item>
                    </Breadcrumb>
                </>
            }
        >

            <DataTable />
        </Panel >
    );
};

export default OrderTable;
