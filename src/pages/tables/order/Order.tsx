import { Box, Tooltip, Typography, useMediaQuery } from '@mui/material'
import { Button, Form, Image, Input, Modal, Select, Tag } from 'antd'
import React, { useEffect, useState } from 'react'
import { Steps } from 'antd';
import axios from 'axios';
import { url } from '../../../url';
import { InfoCircleOutlined, LoadingOutlined } from '@ant-design/icons'
import TextArea from 'antd/es/input/TextArea';
import { notification } from 'antd'
import { RootState } from '../../../store/store';
import { useSelector } from 'react-redux';
import { Driver, Vehicle } from './DataTable';

type NotificationType = 'success' | 'error'

type Permissions = {
    achieved_at: string,
    can_be_achieved: Boolean,
    permission: number,
    permission_number: number
}

type Checkpoints = {
    id: string,
    name: string,
    address: string,
    type: string,
    driver: TDriver,
    vehicle: TVehicle,
    permissions: Permissions[]
}

type TDriver = {
    id: string,
    name: string,
    phone: string,
    avatar_url: string
}

type TVehicle = {
    id: string,
    type: string,
    plate_number: string,
    image_url: string
}

type Order = {
    id: number;
    code: string;
    checkpoints: Checkpoints[],
    height: number,
    length: number,
    weight: number,
    width: number,
    note: string,
    package_types: number[],
    package_value: number,
    receiver_name: string,
    receiver_phone: string,
    sender_name: string,
    sender_phone: string,
    is_confirmed: Boolean,
    end_partner_id: string,
    second_partner_revenue: number,
    first_partner_revenue: number,
    delivery_price: number,
    revenue: number,
    onChange: () => void,
    role: string,
    package_image_url: string,
    delivered_image_url: string,
}

const OrderComponent = (props: Order) => {
    const {
        id,
        code,
        checkpoints,
        height,
        length,
        width,
        weight,
        note,
        package_types,
        package_value,
        receiver_name,
        receiver_phone,
        sender_name,
        sender_phone,
        is_confirmed,
        onChange,
        role,
        end_partner_id,
        delivery_price,
        first_partner_revenue,
        second_partner_revenue,
        revenue,
        package_image_url,
        delivered_image_url
    } = props



    const [hub, setHub] = useState<Checkpoints>()
    const [currentStatus, setCurrentStatus] = useState<number>()
    const [isLoadingDelivered, setIsLoadingDelivered] = useState<Boolean>(false)
    const [isLoadingDelivering, setIsLoadingDelivering] = useState<Boolean>(false)
    const [visibleModal, setVisibleModal] = useState<boolean>(false)
    const [form] = Form.useForm()
    const [api, contextHolder] = notification.useNotification()
    const isNonMobile = useMediaQuery("(min-width:600px)");
    const [visibleModalConfirmLeaveHub, setVisibleModalConfirmLeaveHub] = useState<boolean>(false)
    const [partners, setPartners] = useState<any[]>([])
    const [drivers, setDrivers] = useState<Driver[]>([])
    const [vehicles, setVehicles] = useState<Vehicle[]>([])
    const [openDetail, setOpenDetail] = useState(false)
    const [openImage, setOpenImage] = useState(false)
    const [opeVehicleImage, setOpenVehicleImage] = useState(false)

    console.log(checkpoints[0].driver);


    const openNotificationWithIcon = (type: NotificationType, error?: any, data?: any) => {
        notification[type]({
            message: 'Báo mất đơn hàng',
            description: type === `success` ? `Thông tin đơn hàng đã được cập nhật` : "Có lỗi xảy ra, vui lòng thao tác lại"
        })
    }

    const openNotificationUpdateToDelivered = (type: NotificationType) => {
        notification[type]({
            message: 'Cập nhật trạng thái đơn hàng',
            description: type === `success` ? `Đơn hàng đã đến hub ${checkpoints[1]?.name}` : "Có lỗi xảy ra, vui lòng thao tác lại"
        })
    }
    const openNotificationUpdateToDelivering = (type: NotificationType, error?: any, data?: any) => {
        notification[type]({
            message: 'Cập nhật trạng thái đơn hàng',
            description: type === `success` ? `Đang hàng đã rời khỏi hub` : "Có lỗi xảy ra, vui lòng thao tác lại"
        })
    }


    React.useEffect(() => {
        setHub(checkpoints.find((item) => item.type === 'hub'))
    }, [])

    React.useEffect(() => {
        let getCurrentState = () => {
            if (checkpoints.length === 3) {
                if (checkpoints[2].permissions.find((item) => item.permission === 4)?.achieved_at !== null) {
                    return 5
                }
                if (checkpoints[1].permissions.find((item) => item.permission === 2)?.achieved_at !== null) {
                    return 4
                }
                if (checkpoints[1].permissions.find((item) => item.permission === 3)?.achieved_at !== null) {
                    return 3
                }
                if (checkpoints[0].permissions.find((item) => item.permission === 2)?.achieved_at !== null) {
                    return 2
                }
                if (checkpoints[0].permissions.find((item) => item.permission === 1)?.achieved_at !== null) {
                    return 1
                }
                if (is_confirmed === false) {
                    return 0
                }
            }
            if (checkpoints.length === 2) {
                if (checkpoints[1].permissions.find((item) => item.permission === 4)?.achieved_at !== null) {
                    return 4
                }
                if (checkpoints[1].permissions.find((item) => item.permission === 3)?.achieved_at !== null) {
                    return 3
                }
                if (checkpoints[0].permissions.find((item) => item.permission === 2)?.achieved_at !== null) {
                    return 2
                }
                if (checkpoints[0].permissions.find((item) => item.permission === 1)?.achieved_at !== null) {
                    return 1
                }

                if (is_confirmed === false) {
                    return 0
                }
            }

        }
        setCurrentStatus(getCurrentState)
    }, [props])
    const bearerToken = localStorage.getItem('sessionId')
    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`

    }

    const handleUpdateToDelivered = async () => {
        setIsLoadingDelivered(true)
        try {
            axios({
                method: 'put',
                headers: headers,
                url: `${url}/orders/${code}/status/delivered`
            }).then((res) => {
                onChange()
                openNotificationUpdateToDelivered("success")
                setTimeout(() => {
                    setIsLoadingDelivered(false)
                }, 1000)

            }).catch((error) => {
                console.log(error);
                setIsLoadingDelivered(false)

            })
        } catch (error) {
            console.log(error);
            setIsLoadingDelivered(false)

        }
    }

    const handleUpdateToDelivering = async (vehicleId: string, driverId: string) => {
        setVisibleModalConfirmLeaveHub(false)
        setIsLoadingDelivering(true)
        const data = {
            driver_id: driverId,
            vehicle_id: vehicleId
        }
        try {
            axios({
                method: 'put',
                headers: headers,
                url: `${url}/orders/${code}/status/delivering`,
                data: JSON.stringify(data)
            }).then((res) => {
                onChange()
                openNotificationUpdateToDelivering("success")
                setTimeout(() => {
                    setIsLoadingDelivering(false)
                }, 1000)

            }).catch((error) => {
                console.log(error);
                setIsLoadingDelivering(false)
            })
        } catch (error) {
            console.log(error);
            setIsLoadingDelivering(false)
        }
    }

    const handleChangePartner = (e: any) => {
        form.resetFields(['driver', 'vehicle'])
        try {
            axios({
                method: 'get',
                headers: headers,
                url: `${url}/partners/${end_partner_id}/vehicles`
            }).then((res) => {
                let vehicles = res.data.data.map((item: any) => {
                    return {
                        id: item.id,
                        image_url: item.attributes.image_url,
                        partner_name: item.attributes.partner_name,
                        plate_number: item.attributes.plate_number,
                        type: item.attributes.type
                    }
                })
                setVehicles(vehicles)
            }).catch((error) => {
                console.log(error);
            })
        } catch (error) {
            console.log(error);
        }
        try {
            axios({
                method: 'get',
                headers: headers,
                url: `${url}/partners/${end_partner_id}/drivers`
            }).then((res) => {
                let drivers = res.data.data.map((item: any) => {
                    return {
                        id: item.id,
                        avatar_url: item.attributes.avatar_url,
                        partner_name: item.attributes.partner_name,
                        name: item.attributes.name,
                        phone: item.attributes.phone
                    }
                })
                setDrivers(drivers)
            }).catch((error) => {
                console.log(error);
            })
        } catch (error) {
            console.log(error);
        }
    }


    const handleChange = (value: number) => {

        if ((value !== 3 && value !== 4)) {
            console.log('hong dc dau');
        }
        else if (value === 3) {
            if (checkpoints[1].permissions.find((item => item.permission === 3))?.can_be_achieved === true) {
                handleUpdateToDelivered()
            }

            else {
                console.log('k dc dau');
            }

        }
        else if (value === 4) {
            if (checkpoints[1].permissions.find((item) => item.permission === 2)?.can_be_achieved === true &&
                checkpoints[1].permissions.find((item) => item.permission === 3)?.can_be_achieved === false) {
                setVisibleModalConfirmLeaveHub(true)
                try {
                    axios({
                        method: 'get',
                        headers: headers,
                        url: `${url}/account/partners`,
                        params: {
                            page: 1,
                            per_page: 100
                        }
                    }).then((res) => {
                        console.log(res.data.data);

                        let data = res.data.data.filter((item: any) => item.id == end_partner_id).map((item: any) => {
                            return {
                                id: item.id,
                                name: item.attributes.name
                            }
                        })
                        console.log(data[0]);

                        setPartners(data)
                    }).catch((error) => { })
                } catch (error) {

                }
            }

            else {
                console.log('k dc dau');

            }
        }

    };



    const handleSubmitForm = (value: any) => {
        const data = {
            reason: value.reason
        }
        try {
            axios({
                method: "put",
                url: `${url}/orders/${code}/status/lost`,
                headers: headers,
                data: JSON.stringify(data)
            }).then((res) => {
                console.log(res);
                openNotificationWithIcon("success")
                onChange()
            }).catch((error) => {
                console.log(error);
                openNotificationWithIcon("error")
            })
        } catch (error) {
            console.log(error);
            openNotificationWithIcon("error")
        }
        console.log(value.reason);
        setVisibleModal(false)
        form.resetFields()
    }
    const handleSubmitFormConfirm = (value: any) => {
        handleUpdateToDelivering(value.vehicle, value.driver)
    }

    const mapType = (i: number) => {
        switch (i) {
            case 0: return 'Hàng bình thường'
            case 1: return 'Thực phẩm'
            case 2: return 'Hóa chất'
            case 3: return 'Giấy tờ'
            case 4: return 'Đồ điện từ'
            case 5: return 'hàng dễ vỡ'
        }
    }

    const mapColor = (i: number) => {
        switch (i) {
            case 0: return 'blue'
            case 1: return 'orange'
            case 2: return 'purple'
            case 3: return 'green'
            case 4: return 'cyan'
            case 5: return 'red'
        }
    }



    return (
        <>
            {contextHolder}

            <Box bgcolor={'#f5fcfc'} borderRadius={'8px'} boxShadow={2} p="1rem" display={'flex'} flexDirection={'column'} gap="0.5rem" >
                <Box mb="1rem" display='flex' justifyContent={role === 'admin   ' ? 'space-around' : 'space-between'} alignItems={'center'} gap="0.5rem">
                    <Box flexBasis={'65%'}>
                        <Box display={'flex'} gap="0.5rem">
                            <Typography color="black" fontWeight={'bold'} variant='h5' >
                                Mã đơn hàng: {code}
                            </Typography>
                            {currentStatus && currentStatus >= 1 ? (
                                <InfoCircleOutlined onClick={() => setOpenImage(true)} style={{ fontSize: 30 }} />
                            ) : (<div></div>)}
                        </Box>
                    </Box>
                    {
                        role === 'admin' && checkpoints.length === 3 && (
                            <Box display={'flex'} gap="0.75rem" flexWrap={'wrap'} justifyContent={'flex-end'} >
                                <Box flexBasis={''} display={'flex'} gap="0.25rem">
                                    <Typography color={'black'}>Giá trị đơn hàng: </Typography>
                                    <Typography color={'#1675e0'} fontWeight={'bold'}>{delivery_price.toLocaleString('en-US')}VNĐ</Typography>
                                </Box>
                                <Box flexBasis={''} display={'flex'} gap="0.25rem">
                                    <Typography color={'black'}>Doanh thu chành 1: </Typography>
                                    <Typography color={'#1675e0'} fontWeight={'bold'}>{first_partner_revenue.toLocaleString('en-US')}VNĐ</Typography>
                                </Box>
                                <Box flexBasis={''} display={'flex'} gap="0.25rem">
                                    <Typography color={'black'}>Doanh thu hệ thống: </Typography>
                                    <Typography color={'#1675e0'} fontWeight={'bold'}>{revenue.toLocaleString('en-US')}VNĐ</Typography>
                                </Box>
                                <Box flexBasis={''} display={'flex'} gap="0.25rem">
                                    <Typography color={'black'}>Doanh thu chành 2: </Typography>
                                    <Typography color={'#1675e0'} fontWeight={'bold'}>{second_partner_revenue.toLocaleString('en-US')}VNĐ</Typography>
                                </Box>

                            </Box>
                        )
                    }
                    {
                        role === 'admin' && checkpoints.length === 2 && (
                            <Box display={'flex'} gap="0.75rem" flexWrap={'wrap'} justifyContent={'flex-end'} >
                                <Box display={'flex'} gap="0.25rem">
                                    <Typography color={'black'}>Giá trị đơn hàng: </Typography>
                                    <Typography color={'#1675e0'} fontWeight={'bold'}>{delivery_price.toLocaleString('en-US')}VNĐ</Typography>
                                </Box>
                                <Box display={'flex'} gap="0.25rem">
                                    <Typography color={'black'}>Doanh thu chành xe: </Typography>
                                    <Typography color={'#1675e0'} fontWeight={'bold'}>{first_partner_revenue.toLocaleString('en-US')}VNĐ</Typography>
                                </Box>
                                <Box display={'flex'} gap="0.25rem">
                                    <Typography color={'black'}>Doanh thu hệ thống: </Typography>
                                    <Typography color={'#1675e0'} fontWeight={'bold'}>{revenue.toLocaleString('en-US')}VNĐ</Typography>
                                </Box>
                            </Box>
                        )
                    }
                    {checkpoints.length === 3 && role === 'staff' && (
                        <Button disabled={currentStatus !== 3} onClick={() => setVisibleModal(true)} type="primary" danger>Báo mất đơn hàng</Button>

                    )}
                </Box>
                <Box display='flex' alignItems={'center'} justifyContent={'flex-start'} flexDirection={isNonMobile ? 'row' : 'column'}>
                    <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Điểm xuất phát:
                        </Typography>
                        <Tooltip placement="top" arrow title={checkpoints[0].name}>
                            <Typography>

                                {checkpoints[0].name.length > 15 ? `${checkpoints[0].name.slice(0, isNonMobile ? 25 : 15)}...` : checkpoints[0].name}
                            </Typography>
                        </Tooltip>
                    </Box>
                    {checkpoints.length === 3 && (
                        <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">
                            <Typography color="black" fontWeight={'bold'}>
                                Hub trung chuyển:
                            </Typography>
                            <Tooltip placement="top" arrow title={checkpoints[checkpoints.length - 2].name}>
                                <Typography>

                                    {checkpoints[checkpoints.length - 2].name.length > 15 ? `${checkpoints[checkpoints.length - 2].name.slice(0, isNonMobile ? 50 : 15)}...` : checkpoints[checkpoints.length - 2].name}
                                </Typography>
                            </Tooltip>
                        </Box>
                    )}
                    <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Điểm nhận hàng:
                        </Typography>
                        <Tooltip placement="top" arrow title={checkpoints[checkpoints.length - 1].name}>
                            <Typography>

                                {checkpoints[checkpoints.length - 1].name.length > 15 ? `${checkpoints[checkpoints.length - 1].name.slice(0, isNonMobile ? 20 : 15)}...` : checkpoints[checkpoints.length - 1].name}
                            </Typography>
                        </Tooltip>
                    </Box>


                </Box>
                <Box display='flex' alignItems={'center'} justifyContent={'flex-start'} flexDirection={isNonMobile ? 'row' : 'column'}>
                    <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Địa chỉ:
                        </Typography>
                        <Tooltip placement="top" arrow title={checkpoints[0].address}>
                            <Typography>

                                {checkpoints[0].address.length > 15 ? `${checkpoints[0].address.slice(0, isNonMobile ? 25 : 15)}...` : checkpoints[0].address}
                            </Typography>
                        </Tooltip>
                    </Box>
                    {checkpoints.length === 3 && (
                        <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">
                            <Typography color="black" fontWeight={'bold'}>
                                Địa chỉ:
                            </Typography>
                            <Tooltip placement="top" arrow title={checkpoints[checkpoints.length - 2].address}>
                                <Typography>

                                    {checkpoints[checkpoints.length - 2].address.length > 15 ? `${checkpoints[checkpoints.length - 2].address.slice(0, isNonMobile ? 30 : 15)}...` : checkpoints[checkpoints.length - 2].address}
                                </Typography>
                            </Tooltip>
                        </Box>
                    )}
                    <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Địa chỉ:
                        </Typography>
                        <Tooltip placement="top" arrow title={checkpoints[checkpoints.length - 1].address}>

                            <Typography>
                                {checkpoints[checkpoints.length - 1].address.length > 15 ? `${checkpoints[checkpoints.length - 1].address.slice(0, isNonMobile ? 25 : 15)}...` : checkpoints[checkpoints.length - 1].address}
                            </Typography>
                        </Tooltip>
                    </Box>


                </Box>
                <Box display='flex' justifyContent={'flex-start'} alignItems={'center'} flexDirection={isNonMobile ? 'row' : 'column'}>
                    <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Người gửi:
                        </Typography>
                        <Typography>
                            {sender_name}
                        </Typography>

                    </Box>
                    {checkpoints.length === 3 && (
                        <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">

                        </Box>
                    )}
                    <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Người nhận:
                        </Typography>
                        <Typography>
                            {receiver_name}
                        </Typography>

                    </Box>
                </Box>
                <Box display='flex' justifyContent={'flex-start'} alignItems={'center'} flexDirection={isNonMobile ? 'row' : 'column'}>
                    <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Số điện thoại người gửi:
                        </Typography>

                        <Typography>
                            {sender_phone.replace(/(\d{3})(\d{3})(\d{4})/, "$1 $2 $3")}
                        </Typography>
                    </Box>
                    {checkpoints.length === 3 && (
                        <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">

                        </Box>
                    )}
                    <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Số điện thoại người nhận:
                        </Typography>

                        <Typography>
                            {receiver_phone.replace(/(\d{3})(\d{3})(\d{4})/, "$1 $2 $3")}
                        </Typography>
                    </Box>
                </Box>
                <Box display='flex' justifyContent={'flex-start'} alignItems={'center'} flexDirection={isNonMobile ? 'row' : 'column'}>
                    <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Tài xế:
                        </Typography>

                        <Typography>
                            {checkpoints && checkpoints[1].driver?.name || 'Đang cập nhật'}
                        </Typography>
                        <InfoCircleOutlined onClick={() => setOpenDetail(true)} />
                    </Box>


                </Box>
                <Box display='flex' justifyContent={'flex-start'} alignItems={'center'} flexDirection={isNonMobile ? 'row' : 'column'}>
                    <Box display='flex' flex={1} justifyContent={'flex-start'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Phương tiện:
                        </Typography>

                        <Typography>
                            {checkpoints && checkpoints[1].vehicle?.plate_number || 'Đang cập nhật'}
                        </Typography>
                        <InfoCircleOutlined onClick={() => setOpenVehicleImage(true)} />
                    </Box>


                </Box>
                <Box my="1rem" gap="1rem" display='flex' flexDirection={isNonMobile ? 'row' : 'column'} justifyContent={'space-between'} alignItems={'center'}>
                    <Box display='flex' justifyContent={'center'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Kích thước (cm):
                        </Typography>
                        <Typography>
                            {length / 10} x {width / 10} x {height / 10}
                        </Typography>
                    </Box>
                    <Box display='flex' justifyContent={'center'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Khối lượng (kg):
                        </Typography>
                        <Typography>
                            {weight / 1000}
                        </Typography>
                    </Box>
                    <Box display='flex' justifyContent={'center'} alignItems={'center'} gap="0.25rem">
                        <Typography color="black" fontWeight={'bold'}>
                            Giá trị đơn hàng được khai báo:
                        </Typography>
                        <Typography>
                            {package_value.toLocaleString('en-US')} VND
                        </Typography>
                    </Box>
                    <Box display='flex' flexBasis={'30%'} justifyContent={'center'} alignItems={'center'} gap="0.25rem" flexWrap={'wrap'}>
                        <Typography color="black" fontWeight={'bold'}>
                            Loại hàng hóa:
                        </Typography>

                        {
                            package_types.map((item) => {
                                return (
                                    <Tag color={mapColor(item)} >
                                        {mapType(item)}
                                    </Tag>
                                )
                            })
                        }

                    </Box>
                </Box>
                {checkpoints.length === 3 ? (
                    <Steps
                        size="small"
                        current={currentStatus}
                        onChange={handleChange}
                        items={isLoadingDelivered === true ? [
                            {
                                title: 'Đã tạo đơn',
                            },
                            {
                                title: 'Đã nhận đơn',

                            },
                            {
                                title: <Tooltip title="Đang trên đường đến hub">
                                    <p>Đang trên đường đến...</p>
                                </Tooltip>,

                            },
                            {
                                title: `Đang ở hub ${checkpoints[1].name}`,
                                icon: <LoadingOutlined />
                            },
                            {
                                title: 'Xuất phát đến điểm cuối',
                            },
                            {
                                title: 'Hoàn thành',
                            },
                        ] : (isLoadingDelivering === true ? [

                            {
                                title: 'Đã tạo đơn',
                            },
                            {
                                title: 'Đã nhận đơn',


                            },
                            {
                                title: <Tooltip title="Đang trên đường đến hub">
                                    <p>Đang trên đường đến...</p>
                                </Tooltip>,

                            },
                            {
                                title: `Đang ở hub ${checkpoints[1].name}`,
                            },
                            {
                                title: 'Xuất phát đến điểm cuối',
                                icon: <LoadingOutlined />
                            },
                            {
                                title: 'Hoàn thành',
                            },

                        ] : [
                            {
                                title: 'Đã tạo đơn',
                            },
                            {
                                title: 'Đã nhận đơn',

                            },
                            {
                                title: <Tooltip title="Đang trên đường đến hub">
                                    <p>Đang trên đường đến...</p>
                                </Tooltip>,

                            },
                            {
                                title: `Đang ở hub ${checkpoints[1].name}`,
                            },
                            {
                                title: 'Xuất phát đến điểm cuối',
                            },
                            {
                                title: 'Hoàn thành',
                            },
                        ])}
                    />
                ) : (
                    <Steps
                        size="small"
                        current={currentStatus}
                        items={[
                            {
                                title: 'Đã tạo đơn',
                            },
                            {
                                title: 'Đã nhận đơn',
                            },
                            {
                                title: <Tooltip title="Đang trên đường đến trạm">
                                    <p>Đang trên đường đến...</p>
                                </Tooltip>,

                            },
                            {
                                title: 'Đã đến trạm',
                            },
                            {
                                title: 'Đã giao hàng thành công',
                            },

                        ]}
                    />
                )}

            </Box>
            <Modal
                title={`Báo mất đơn hàng ${code}`}
                footer={false}
                onCancel={() => setVisibleModal(false)}
                open={visibleModal}
                width={500}
            >
                <Form form={form} onFinish={handleSubmitForm}>
                    {/* TÊN */}
                    <p className="mb-1 ">Lý do / Nguyên nhân:</p>

                    <Form.Item name={"reason"}>
                        <TextArea rows={4} />
                    </Form.Item>

                    <div
                        style={{
                            width: "100%",
                            display: "flex",
                            justifyContent: "right",
                            height: "35px",
                        }}
                    >
                        <Form.Item>
                            <Button
                                type="primary"
                                className="bg-[#1677ff]"
                                htmlType="submit"
                                style={{ marginRight: "15px" }}
                            >
                                Báo mất
                            </Button>
                        </Form.Item>

                        <Button onClick={() => setVisibleModal(false)} className="bg-[#f72a2a] text-white" type="default">
                            Hủy
                        </Button>
                    </div>
                </Form>
            </Modal>

            <Modal
                title={`Xác nhận đơn hàng ${code} đã rời khỏi hub`}
                footer={false}
                onCancel={() => setVisibleModalConfirmLeaveHub(false)}
                open={visibleModalConfirmLeaveHub}
                width={500}
            >
                <Form form={form} onFinish={handleSubmitFormConfirm}>
                    <p className="mb-1 ">Nhà xe:</p>

                    <Form.Item name={"partners"}>
                        <Select onChange={handleChangePartner} options={partners && partners.map((item) => {
                            return {
                                value: item.id,
                                label: item.name
                            }
                        })} />
                    </Form.Item>
                    <p className="mb-1 ">Biển số xe:</p>

                    <Form.Item name={"vehicle"}>
                        <Select options={vehicles && vehicles.map((item) => {
                            return {
                                value: item.id,
                                label: item.plate_number
                            }
                        })} />
                    </Form.Item>
                    <p className="mb-1 ">Tài xế:</p>

                    <Form.Item name={"driver"}>
                        <Select options={drivers && drivers.map((item) => {
                            return {
                                value: item.id,
                                label: item.name
                            }
                        })} />
                    </Form.Item>
                    <div
                        style={{
                            width: "100%",
                            display: "flex",
                            justifyContent: "right",
                            height: "35px",
                        }}
                    >
                        <Form.Item>
                            <Button
                                type="primary"
                                className="bg-[#1677ff]"
                                htmlType="submit"
                                style={{ marginRight: "15px" }}
                            >
                                Xác nhận
                            </Button>
                        </Form.Item>

                        <Button onClick={() => setVisibleModalConfirmLeaveHub(false)} className="bg-[#f72a2a] text-white" type="default">
                            Hủy
                        </Button>
                    </div>
                </Form>
            </Modal>
            <Modal
                title={`Thông tin tài xế`}
                footer={false}
                onCancel={() => setOpenDetail(false)}
                open={openDetail}
                width={500}
            >
                <Box display={'flex'} gap={'1.5rem'} alignItems={'center'}  >
                    <Image src={checkpoints[1].driver?.avatar_url} style={{ width: 150, height: 150 }} />
                    <Box display={'flex'} flexDirection={'column'} gap="0.25rem">
                        <Typography> Tên tài xế: {checkpoints[1].driver?.name || 'Đang cập nhật'} </Typography>
                        <Typography> Số điện thoại: {checkpoints[1].driver?.phone || 'Đang cập nhật'} </Typography>


                    </Box>
                </Box>
            </Modal>
            <Modal
                title={`Thông tin đơn hàng ${code}`}
                footer={false}
                onCancel={() => setOpenImage(false)}
                open={openImage}
                width={800}
            >
                <Box display={'flex'} justifyContent={'space-between'} >
                    <Box display={'flex'} flexDirection={'column'}>
                        <Typography fontWeight={'bold'}>
                            Hình ảnh món hàng

                        </Typography>
                        <Image src={package_image_url} height={300} width={300} />
                    </Box>
                    <Box display={'flex'} flexDirection={'column'}>
                        <Typography fontWeight={'bold'}>

                            Hình ảnh xác nhận giao hàng
                        </Typography>

                        <Image src={delivered_image_url} height={300} width={300} />
                    </Box>
                </Box>
            </Modal>
            <Modal
                title={`Hình ảnh phương tiện`}
                footer={false}
                onCancel={() => setOpenVehicleImage(false)}
                open={opeVehicleImage}
            >
                <Box display={'flex'} gap={'1.5rem'} alignItems={'center'}  >
                    <Image src={checkpoints[1].vehicle?.image_url} style={{ height: 200 }} />
                    <Box display={'flex'} flexDirection={'column'} gap="0.25rem">
                        <Typography> Loại xe: {checkpoints[1].vehicle?.type || 'Đang cập nhật'} </Typography>
                        <Typography> Biển số xe: {checkpoints[1].vehicle?.plate_number || 'Đang cập nhật'} </Typography>


                    </Box>
                </Box>
            </Modal>
        </>
    )
}

export default OrderComponent