import React, { useEffect, useState } from 'react';
import {
    Input,
    InputGroup,
    Table,
    Button,
    DOMHelper,
    Progress,
    Checkbox,
    Stack,
    SelectPicker,
    Modal,
    Form
} from 'rsuite';
import SearchIcon from '@rsuite/icons/Search';
import MoreIcon from '@rsuite/icons/legacy/More';
import DrawerView from './DrawerView';
import axios from 'axios';
import { url } from '../../../url'
import { Box, Typography } from '@mui/material';
import { useSelector } from 'react-redux';
import { RootState } from '../../../store/store';
import { notification } from 'antd';
import { useMemo } from 'react';
import {
    MaterialReactTable,
    type MRT_ColumnDef,
} from 'material-react-table';
import { useGetBoxSizeQuery, useLazyGetBoxSizeQuery } from '../../../services/apiSlice';

type NotificationType = 'success' | 'error'



const { Column, HeaderCell, Cell } = Table;
const { getHeight } = DOMHelper;

type BoxSize = {
    id: string;
    type?: string;
    max_length: number,
    max_width: number,
    max_height: number,
    max_weight: number
}

const DataTable = () => {
    const [showDrawer, setShowDrawer] = useState(false);
    const [selectedId, setSelectedId] = useState('')
    const [currentTable, setCurrentTable] = useState<BoxSize[]>([])
    const [currentDataView, setCurrentDataView] = useState<any[]>([]);
    const [isLoadingDetail, setIsLoadingDetail] = useState(false)
    const [open, setOpen] = useState(false)
    const [length, setLength] = useState<number>(1)
    const [width, setWidth] = useState<number>(1)
    const [height, setHeight] = useState<number>(1)
    const [weight, setWeight] = useState<number>(1)
    const [error, setError] = useState('')
    const [api, contextHolder] = notification.useNotification()


    const { data, isLoading } = useGetBoxSizeQuery({
        page: 1,
        perPage: 100
    })

    const [fetchBoxSize, boxSizeResult, lastPromise] = useLazyGetBoxSizeQuery()
    const openNotificationWithIcon = (type: NotificationType) => {

        api[type]({
            message: 'Tạo mới 1 box size',
            description: type === `success` ? `Tạo mới box size thành công` : ""
        })
    }

    useEffect(() => {
        if (weight <= 0 || height <= 0 || length <= 0 || width <= 0) {
            setError('Giá trị thông số đơn hàng không được nhỏ hơn 0')
        }
        else {
            setError('')

        }
    }, [weight, height, width, length])




    const bearerToken = localStorage.getItem('sessionId')

    const convertWeight = (value: number) => {
        if (value < 1000) {
            return value + "g"
        }
        else {
            return Math.round(value / 1000) + "kg"
        }
    }

    const convertLength = (value: number) => {
        if (value < 1000) {
            return value / 10 + "cm"
        }
        else return (value / 1000).toFixed(2) + "m"
    }
    console.log(bearerToken);

    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`

    }

    // const getBoxSizeList = async () => {
    //     setIsLoading(true)
    //     const params = {
    //         page: 1,
    //         perPage: 100
    //     }
    //     try {
    //         axios({
    //             method: "get",
    //             url: `${url}/tariff/box-sizes`,
    //             headers: headers,
    //             params: params
    //         }).then((res) => {
    //             console.log(res.data.data);
    //             let resData = res.data.data.map((item: any) => {
    //                 return {
    //                     id: item.id,
    //                     type: item.type,
    //                     max_length: item.attributes.max_length,
    //                     max_width: item.attributes.max_width,
    //                     max_height: item.attributes.max_height,
    //                     max_weight: item.attributes.max_weight
    //                 }
    //             })
    //             setCurrentTable(resData)
    //             setIsLoading(false)
    //         }).catch((error) => {
    //             console.log(error);
    //             setIsLoading(true)

    //         })
    //     } catch (error) {
    //         console.log(error);
    //         setIsLoading(true)

    //     }
    // }

    const getBoxSizeById = async (id: string) => {
        setIsLoadingDetail(true)
        try {
            axios({
                method: 'get',
                url: `${url}/tariff/box-sizes/${id}/prices`,
                headers: headers,
            }).then((res) => {
                setCurrentDataView(res.data.data)
                setIsLoadingDetail(false)

            }).catch((error) => {
                console.log(error);

            })
        } catch (error) {
            console.log(error);

        }
    }

    React.useEffect(() => {
        if (data) {
            let resData = data.data.map((item: any) => {
                return {
                    id: item.id,
                    type: item.type,
                    max_length: item.attributes.max_length,
                    max_width: item.attributes.max_width,
                    max_height: item.attributes.max_height,
                    max_weight: item.attributes.max_weight
                }
            })
            setCurrentTable(resData)
        }

    }, [data])

    const handleRowClick = (rowData: any) => {
        getBoxSizeById(rowData.id)
        handleViewBoxSizeDetail(rowData.id)
    }

    const handleViewBoxSizeDetail = (id: string) => {
        setShowDrawer(true)
        setSelectedId(id)
    }

    const handleCrateBoxSize = () => {
        const values = {
            max_height: height * 10,
            max_weight: Math.round(weight * 1000),
            max_length: length * 10,
            max_width: width * 10,
        }

        try {
            axios({
                method: "post",
                url: `${url}/tariff/box-sizes`,
                headers: headers,
                data: JSON.stringify(values)
            }).then((res) => {
                openNotificationWithIcon("success")
                fetchBoxSize({
                    page: 1,
                    perPage: 100
                })
                setOpen(false);

            }).catch((error) => {
                console.log(error);

            })
        } catch (error) {
            console.log(error);

        }
    }





    const columns = useMemo<MRT_ColumnDef<BoxSize>[]>(
        () => [
            {
                accessorKey: 'id',
                header: 'Id',
                size: 100,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();
                    return (
                        <Box  >


                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue}
                            </Typography>

                        </Box>

                    )
                }
            },
            {
                accessorKey: 'max_length',
                header: 'Chiều dài',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();

                    return (
                        <Box  >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{convertLength(parseInt(cellValue))}</Typography>
                        </Box>
                    );
                },
            },
            {
                accessorKey: 'max_width',
                header: 'Chiều rộng',
                size: 150,


                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();


                    return (
                        <Box  >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{convertLength(parseInt(cellValue))}</Typography>
                        </Box>

                    );
                },
            },

            {
                accessorKey: 'max_height',

                header: 'Chiều cao',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();

                    return (
                        <Box  >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{convertLength(parseInt(cellValue))}</Typography>
                        </Box>
                    )
                }
            },
            {
                accessorKey: 'max_weight',
                header: 'Cân nặng',
                size: 150,
                Cell: ({ cell }) => {
                    let cellString = cell.getValue<string>();

                    return (
                        <Box >
                            <Typography textAlign={'center'} fontFamily={'Roboto Mono, Menlo'} color={'black'} fontSize={'14px'} sx={{
                            }} >{convertWeight(parseInt(cellString))}</Typography>
                        </Box>

                    );
                },


            },

        ],
        []
    );

    return (
        <>
            {contextHolder}
            <Stack className="table-toolbar" justifyContent="space-between" style={{
                marginBottom: '1rem'
            }}>
                <Button appearance="primary" onClick={() => { setOpen(true) }}>
                    Thêm 1 box size
                </Button>


            </Stack>

            <MaterialReactTable columns={columns} data={currentTable}
                state={{
                    isLoading: isLoading
                }}
                muiTableBodyRowProps={({ row }) => ({
                    onClick: () => {
                        handleRowClick(row.original)
                    },
                    sx: {
                        transition: 'background-color 0.3s', // Add a transition effect for smoother hover
                        '&:hover': {
                            filter: 'brightness(1.1)',
                        },
                        height: '6px',
                    },
                })}
                muiTableHeadCellProps={{
                    align: 'center'

                }}
                initialState={{
                    pagination: {
                        pageSize: 10,
                        pageIndex: 0,
                    },
                    density: 'compact',
                }}

            />
            <Modal size={'sm'} open={open} onClose={() => { setOpen(false) }}>
                <Modal.Header>
                    <Modal.Title>Tạo mới box size</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form fluid>
                        <Stack justifyContent="space-between" style={{ marginBottom: 20 }}>
                            <Form.Group>
                                <Form.ControlLabel>Cân nặng (kg)</Form.ControlLabel>
                                <Form.Control required type='number' min={1} onChange={(value: any) => { setWeight(value) }} name="max_weight" style={{ width: 200 }} />
                            </Form.Group>
                            <Form.Group>
                                <Form.ControlLabel>Chiều cao (cm)</Form.ControlLabel>
                                <Form.Control required type='number' min={1} onChange={(value: any) => { setHeight(value) }} name="max_height" style={{ width: 200 }} />
                            </Form.Group>
                        </Stack>
                        <Form.Group>
                            <Form.ControlLabel>Chiều dài (cm)</Form.ControlLabel>
                            <Form.Control required type='number' min={1} onChange={(value: any) => { setLength(value) }} name="max_length" />
                        </Form.Group>
                        <Form.Group>
                            <Form.ControlLabel>Chiều rộng (cm)</Form.ControlLabel>
                            <Form.Control required type='number' min={1} onChange={(value: any) => { setWidth(value) }} name="max_width" />
                        </Form.Group>

                        {error.length > 0 && (<Typography color="red">{error}</Typography>)}

                        <Modal.Footer>
                            <Button appearance="primary" onClick={() => {
                                if (weight && length && height && width) {
                                    handleCrateBoxSize()
                                }
                                else {
                                    setError("Vui lòng điền đầy đủ thông tin")
                                }
                            }}>
                                Xác nhận
                            </Button>
                            <Button onClick={() => { setOpen(false) }} appearance="subtle">
                                Bỏ qua
                            </Button>
                        </Modal.Footer>

                    </Form>
                </Modal.Body>
            </Modal>
            <DrawerView boxSizeId={selectedId} isLoading={isLoadingDetail} open={showDrawer} data={currentDataView} onClose={() => setShowDrawer(false)} />
        </>
    );
};

export default DataTable;
