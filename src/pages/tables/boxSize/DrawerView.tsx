import { Box, Tooltip, Typography } from '@mui/material';
import React, { useEffect, useMemo } from 'react';
import {

    DrawerProps,
    Button,
    Form,
    Stack,
    InputNumber,
    InputGroup,
    Slider,
    Rate,
    Table,
    Modal,
} from 'rsuite';


import { Drawer, DatePicker, Tag } from 'antd';
import { url } from '../../../url'
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { increaseCount } from '../../../store/countSlice';
import {
    MaterialReactTable,
    type MRT_ColumnDef,
} from 'material-react-table';
import { setSubmitForm } from '../../../store/booleanSlice';
import { RootState } from '../../../store/store';



const { Column, HeaderCell, Cell } = Table;

type Items = {
    from_kilometer: string,
    to_kilometer: string,
    price_per_kilometer: string,
    min_amount: string,
    max_amount: string
}

type Price = {
    id?: string,
    apply_from: string,
    apply_to: string,
    name: string,
    priority: number,
    note: string,
    items: Items[]
}



const InputItems = ({ onInputChange }: any) => {
    const [items, setItems] = React.useState<Items>({
        from_kilometer: '',
        to_kilometer: '',
        price_per_kilometer: '',
        min_amount: '',
        max_amount: ''
    })
    const [error, setError] = React.useState<string>('')
    const [kilometerBefore, setKilometerBefore] = React.useState<string>('')
    const [count, setCount] = React.useState(0)

    const dispatch = useDispatch();

    const handleInputSubmit = () => {
        onInputChange(items);
        setKilometerBefore(items.to_kilometer)
        setCount(count + 1)
        setItems({
            from_kilometer: '',
            to_kilometer: '',
            price_per_kilometer: '',
            min_amount: '',
            max_amount: ''
        })
    }



    React.useEffect(() => {

        if (count > 0 && parseInt(items.from_kilometer) !== parseInt(kilometerBefore)) {
            setError("Quãng đường tối thiểu phải bằng với quãng đường tối đa trước đó")
        }
        else {
            if (parseInt(items.from_kilometer) > parseInt(items.to_kilometer)) {
                setError('Quãng đường tối thiểu không thể lớn hơn quãng đường tối ta')
            }
            else {
                setError('')
            }
        }

    }, [items.from_kilometer, items.to_kilometer, kilometerBefore, count])

    React.useEffect(() => {
        if (parseInt(items.min_amount) > parseInt(items.max_amount)) {
            setError('Giá tối thiểu không thể lớn hơn giá tối đa')
        }
        else {
            setError('')
        }
    }, [items.min_amount, items.max_amount])

    React.useEffect(() => {
        if (items.from_kilometer.length > 0 && items.to_kilometer.length > 0 && items.min_amount.length > 0 && items.max_amount.length > 0 && items.price_per_kilometer.length > 0) {
            setError('')
            dispatch(setSubmitForm({
                submitForm: false
            }))
        }
        if (items.from_kilometer.length === 0 && items.to_kilometer.length === 0 && items.min_amount.length === 0 && items.max_amount.length === 0 && items.price_per_kilometer.length === 0) {
            dispatch(setSubmitForm({
                submitForm: true
            }))
        }
        else {
            dispatch(setSubmitForm({
                submitForm: false
            }))
        }


    }, [items.from_kilometer, items.to_kilometer, items.max_amount, items.min_amount, items.price_per_kilometer])


    return (
        <Form fluid>
            <Stack justifyContent="space-between" style={{ marginBottom: 20 }}>
                <Form.Group>
                    <Form.ControlLabel>Từ (km)</Form.ControlLabel>
                    <Form.Control value={items.from_kilometer} onChange={(value) => {
                        setItems({
                            ...items,
                            from_kilometer: value
                        })
                    }} required type='number' name="form_kilometer" />
                </Form.Group>
                <Form.Group>
                    <Form.ControlLabel>Đến (km)</Form.ControlLabel>
                    <Form.Control value={items.to_kilometer} onChange={(value) => {
                        setItems({
                            ...items,
                            to_kilometer: value
                        })
                    }} required type='number' name="to_kilometer" />
                </Form.Group>
            </Stack>
            <Stack justifyContent="space-between" style={{ marginBottom: 20 }}>
                <Form.Group>
                    <Form.ControlLabel>Giá tối thiểu (VND)</Form.ControlLabel>
                    <Form.Control value={items.min_amount} onChange={(value) => {
                        setItems({
                            ...items,
                            min_amount: value
                        })
                    }} required type='number' name="min_amount" />
                </Form.Group>
                <Form.Group>
                    <Form.ControlLabel>Giá tối đa (VND)</Form.ControlLabel>
                    <Form.Control value={items.max_amount} onChange={(value) => {
                        setItems({
                            ...items,
                            max_amount: value
                        })
                    }} required type='number' name="max_amount" />
                </Form.Group>
            </Stack>
            <Form.Group>
                <Form.ControlLabel>Đơn giá (VND/km)</Form.ControlLabel>
                <Form.Control value={items.price_per_kilometer} onChange={(value) => {
                    setItems({
                        ...items,
                        price_per_kilometer: value
                    })
                }} required type='number' name="price_per_kilometer" />
            </Form.Group>

            {error.length > 0 && (
                <Typography pb="1rem" color="red" fontSize={'14px'}>{error}</Typography>
            )}
            <Button appearance='primary' onClick={() => {
                if (items.from_kilometer.length <= 0 || items.to_kilometer.length <= 0 || items.min_amount.length <= 0 || items.max_amount.length <= 0 || items.price_per_kilometer.length <= 0) {
                    setError('Vui lòng điền đầy đủ thông tin')
                }
                else if (!error.length && items.from_kilometer.length && items.to_kilometer && items.max_amount && items.min_amount && items.price_per_kilometer) {
                    {
                        handleInputSubmit()
                    }
                }

            }}>Thêm 1 đơn giá</Button>
        </Form>
    )
}

const DrawerView = (props: any) => {
    const { boxSizeId, data, isLoading, onClose, ...rest } = props;
    const [open, setOpen] = React.useState(false)

    const [dataTable, setDataTable] = React.useState<Price[]>([])
    const [price, setPrice] = React.useState<Price>({
        apply_from: '',
        apply_to: '',
        name: '',
        priority: 0,
        note: '',
        items: []
    })

    const [error, setError] = React.useState('')

    const dispatch = useDispatch()
    const canSubmit = useSelector((state: RootState) => state.submitForm.submitForm)

    React.useEffect(() => {

        let responseData = data.map((item: any) => {
            return {
                id: item.id,
                apply_from: item.attributes.apply_from,
                apply_to: item.attributes.apply_to,
                name: item.attributes.name,
                note: item.attributes.note,
                priority: item.attributes.priority,
                items: item.attributes.items
            }
        })


        setDataTable(responseData)


    }, [data])




    const bearerToken = localStorage.getItem('sessionId')

    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`

    }


    const handleCreateNewPriceTable = () => {
        setOpen(true)
    }
    const handleChangeApplyFrom = (date: any, dateString: any) => {
        setPrice({
            ...price,
            apply_from: dateString
        })
    }
    const handleChangeApplyTo = (date: any, dateString: any) => {
        setPrice({
            ...price,
            apply_to: dateString
        })
    }


    const handleViewItems = (values: any) => {
        setPrice({
            ...price,
            items: [
                ...price.items,
                values
            ]
        })
    }

    const handleCreatePrice = async () => {

        try {
            axios({
                method: 'post',
                url: `${url}/tariff/box-sizes/${boxSizeId}/prices`,
                headers: headers,
                data: JSON.stringify(price)
            }).then((res) => {
                setOpen(false)
            }).catch((error) => {
                console.log(error);

            })
        } catch (error) {
            console.log(error);

        }
    }
    React.useEffect(() => {
        const date1 = new Date(price.apply_from)
        const date2 = new Date(price.apply_to)

        if (date1 >= date2) {

            setError('Ngày bắt đầu không thể sau ngày kết thúc')
        }
        else {
            setError('')
        }
    }, [price.apply_from, price.apply_to])

    const columns = useMemo<MRT_ColumnDef<Price>[]>(
        () => [
            {
                accessorKey: 'id',
                header: 'Id',
                maxSize: 30,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();
                    return (
                        <Box >


                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue}
                            </Typography>

                        </Box>

                    )
                }
            },
            {
                accessorKey: 'name',
                header: 'Tên bảng giá',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();
                    if (cellValue.length > 20) {
                        const firstThree = cellValue.slice(0, 15);
                        cellValue = `${firstThree}...`;
                    }
                    return (
                        <Tooltip title={cell.getValue<string>()}>
                            <Box >
                                <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                                }} >{cellValue}</Typography>
                            </Box>
                        </Tooltip>
                    );
                },
            },
            {
                accessorKey: 'items',
                header: 'Mức giá',
                maxSize: 200,
                enableColumnResizing: true,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<Items[]>();

                    return (
                        <Box display={'flex'} flexWrap={'wrap'} >
                            {cellValue.map((item: any, i) => {
                                return (
                                    <Tag key={i} color='processing'>Từ {item.fromKilometer}km đến {item.toKilometer}km, giá tối thiểu: {item.minAmount}VND, giá tối đa: {item.maxAmount}VND </Tag>
                                )
                            })}
                        </Box>
                    );
                },
            },
            {
                accessorKey: 'apply_from',
                header: 'Ngày bắt đầu',
                size: 150,


                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();
                    if (cellValue.length > 20) {
                        const firstThree = cellValue.slice(0, 15);
                        cellValue = `${firstThree}...`;
                    }
                    return (
                        <Tooltip title={cell.getValue<string>()}>
                            <Box >
                                <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                                }} >{cellValue.split('T')[0]}</Typography>
                            </Box>
                        </Tooltip>
                    );
                },
            },

            {
                accessorKey: 'apply_to',

                header: 'Ngày kết thúc',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();
                    if (cellValue.length > 20) {
                        const firstThree = cellValue.slice(0, 15);
                        cellValue = `${firstThree}...`;
                    }
                    return (
                        <Tooltip title={cell.getValue<string>()}>
                            <Box >
                                <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                                }} >{cellValue.split('T')[0]}</Typography>
                            </Box>
                        </Tooltip>
                    );
                }
            },
            {
                accessorKey: 'note',
                header: 'Ghi chú',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();
                    if (cellValue && cellValue.length > 20) {
                        const firstThree = cellValue.slice(0, 15);
                        cellValue = `${firstThree}...`;
                    }
                    return (
                        <Tooltip title={cell.getValue<string>()}>
                            <Box >
                                <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                                }} >{cellValue !== null ? cellValue : 'Không có'}</Typography>
                            </Box>
                        </Tooltip>
                    );
                },
            },



        ],
        []
    );


    return (
        <>
            <Drawer width={1000} title={` Bảng giá box ${boxSizeId} `} backdrop="static" size="lg" placement="right" onClose={onClose} {...rest}>



                <Button appearance="primary" style={{
                    marginBottom: '2rem'
                }} onClick={handleCreateNewPriceTable}>
                    Thêm bảng giá
                </Button>
                <MaterialReactTable columns={columns} data={dataTable}
                    columnResizeMode='onChange'
                    state={{
                        isLoading: isLoading
                    }}
                    muiTableBodyRowProps={({ row }) => ({

                        sx: {
                            transition: 'background-color 0.3s', // Add a transition effect for smoother hover
                            '&:hover': {
                                filter: 'brightness(1.1)',
                            },
                            height: '6px',
                        },
                    })}
                    muiTableHeadCellProps={{
                        align: 'center'
                    }}
                    initialState={{
                        pagination: {
                            pageSize: 10,
                            pageIndex: 0,
                        },
                        density: 'compact',
                    }}

                />
                <Drawer title="Tạo mới một bảng giá" width={1000} open={open} onClose={() => setOpen(false)}>
                    <Form fluid>
                        <Stack justifyContent="space-between" style={{ marginBottom: 20 }}>
                            <Form.Group>
                                <Form.ControlLabel>Ngày bắt đầu</Form.ControlLabel>
                                <DatePicker name="start_date" onChange={handleChangeApplyFrom} format={"YYYY/MM/DD"} style={{ width: 260 }} />

                            </Form.Group>
                            <Form.Group>
                                <Form.ControlLabel>Ngày kết thúc</Form.ControlLabel>
                                <DatePicker onChange={handleChangeApplyTo} name="apply_to_date" format={"YYYY/MM/DD"} style={{ width: 260 }} />

                            </Form.Group>
                            <Form.Group>
                                <Form.ControlLabel>Độ ưu tiên</Form.ControlLabel>
                                <Form.Control onChange={(value) => {
                                    setPrice({
                                        ...price,
                                        priority: value
                                    })
                                }} required type='number' name="priority" />
                            </Form.Group>
                        </Stack>
                        <Form.Group>
                            <Form.ControlLabel>Tên bảng giá</Form.ControlLabel>
                            <Form.Control onChange={(value) => {
                                setPrice({
                                    ...price,
                                    name: value
                                }
                                )
                            }} required type='' name="name" />
                        </Form.Group>
                        <Form.Group>
                            <Form.ControlLabel>Ghi chú</Form.ControlLabel>
                            <Form.Control required type='' onChange={(value) => {
                                setPrice({
                                    ...price,
                                    note: value
                                }
                                )
                            }} name="note" />
                        </Form.Group>
                        <Form.Group>
                            <Form.ControlLabel>Bảng giá</Form.ControlLabel>
                            <Box display={'flex'} flexWrap={'nowrap'} gap="0.5rem">
                                {price && (
                                    <>
                                        {price.items.map((item, i) => {
                                            return (
                                                <Tag key={i} color='processing'>Từ {item.from_kilometer}km đến {item.to_kilometer}km, giá tối thiểu: {item.min_amount}VND, giá tối đa: {item.max_amount}VND </Tag>

                                            )
                                        })}
                                    </>
                                )}
                            </Box>
                            <InputItems onInputChange={handleViewItems} />
                        </Form.Group>
                        {error.length > 0 && (
                            <Typography color="red" fontSize={'15px'}>{error}</Typography>
                        )}
                        <Modal.Footer>
                            <Button onClick={() => {
                                if (!error.length && canSubmit === true && price.items.length > 0) {
                                    handleCreatePrice()
                                }

                            }} type='submit' appearance="primary" >
                                Xác nhận
                            </Button>
                            <Button onClick={() => { setOpen(false) }} appearance="subtle">
                                Bỏ qua
                            </Button>
                        </Modal.Footer>

                    </Form>
                </Drawer>

            </Drawer>

        </>
    );
};

export default DrawerView;
