import React from 'react';
import { Breadcrumb, Panel } from 'rsuite';
import DataTable from './DataTable';

const BoxSizeTable = () => {
    return (
        <Panel
            header={
                <>
                    <h3 className="title">Danh sách box size</h3>
                    <Breadcrumb>
                        <Breadcrumb.Item style={{ fontSize: '16px' }} href="/">Trang chủ</Breadcrumb.Item>
                        <Breadcrumb.Item style={{ fontSize: '16px' }}>Bảng</Breadcrumb.Item>
                        <Breadcrumb.Item style={{ fontSize: '16px' }} active>Danh sách box size</Breadcrumb.Item>
                    </Breadcrumb>
                </>
            }
        >
            <DataTable />
        </Panel>
    );
};

export default BoxSizeTable;
