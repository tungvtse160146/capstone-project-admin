import React, { useMemo, useState } from 'react';
import {
    Input,
    InputGroup,
    Table,
    Button,
    DOMHelper,
    Progress,
    Checkbox,
    Stack,
    SelectPicker
} from 'rsuite';
import SearchIcon from '@rsuite/icons/Search';
import MoreIcon from '@rsuite/icons/legacy/More';
import axios from 'axios';
import { url } from '../../../url'
import { Box, MenuItem, Typography } from '@mui/material';
import { useDispatch } from 'react-redux';
import {
    MaterialReactTable,
    type MRT_ColumnDef,
} from 'material-react-table';
import { Tag } from 'antd';
import { notification } from 'antd'

type NotificationType = 'success' | 'error'



type Staff = {
    id: string,
    email: string,
    username: string,
    hub_name: string,
    hub_address: string,
    status: number,
}



const DataTable = () => {
    const [searchKeyword, setSearchKeyword] = useState('');
    const [data, setData] = useState<any[]>([])
    const [isLoading, setIsLoading] = useState(false);
    const [dataTable, setDataTable] = useState<Staff[]>([])

    const dispatch = useDispatch()

    const [api, contextHolder] = notification.useNotification()



    const openNotificationWithIcon = (type: NotificationType, error?: any, data?: any) => {

        api[type]({
            message: 'Cập nhật trạng thái nhân viên',
            description: type === `success` ? `Cập nhật thành công` : "Có lỗi xảy ra, vui lòng thao tác lại"
        })
    }


    const bearerToken = localStorage.getItem('sessionId')
    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`

    }

    const getStaffList = async () => {
        setIsLoading(true)
        try {
            axios({

                method: 'get',
                url: `${url}/account/staffs`,
                headers: headers
            }).then((res) => {
                console.log(res.data.data);
                setData(res.data.data)
                setIsLoading(false)

            }).catch((error) => {
                console.log(error);
                setIsLoading(false)
            })
        } catch (error) {
            console.log(error);
            setIsLoading(false)

        }
    }

    React.useEffect(() => {
        let resData = data.map((item) => {
            return {
                id: item.id,
                email: item.attributes.email,
                username: item.attributes.username,
                hub_name: item.attributes.hub.name,
                hub_address: item.attributes.hub.address,
                status: item.attributes.status
            }
        })
        setDataTable(resData)
    }, [data])

    React.useEffect(() => {
        getStaffList()
    }, [])

    const handleUpdateStatusStaff = (id: string, status: number) => {
        let updateStatus = 0
        if (status === 0) {
            updateStatus = 1
        }

        try {
            axios({
                method: "PUT",
                headers: headers,
                url: `${url}/account/staffs/${id}/status/${updateStatus}`
            }).then((res) => {
                console.log(res);
                getStaffList()
                openNotificationWithIcon("success")
            }).catch((error) => {
                console.log(error);
                openNotificationWithIcon("error")

            })
        } catch (error) {
            console.log(error);
            openNotificationWithIcon("error")

        }
    }

    const columns = useMemo<MRT_ColumnDef<Staff>[]>(
        () => [
            {
                accessorKey: 'id',
                header: 'Id',
                size: 100,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();
                    return (
                        <Box  >


                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue}
                            </Typography>

                        </Box>

                    )
                }
            },
            {
                accessorKey: 'email',
                header: 'Email',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();

                    return (
                        <Box  >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue}</Typography>
                        </Box>
                    );
                },
            },
            {
                accessorKey: 'username',
                header: 'Tài khoản',
                size: 150,


                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();


                    return (
                        <Box  >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue}</Typography>
                        </Box>

                    );
                },
            },

            {
                accessorKey: 'hub_name',

                header: 'Tên trạm',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();

                    return (
                        <Box  >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue}</Typography>
                        </Box>
                    )
                }
            },
            {
                accessorKey: 'hub_address',
                header: 'Địa chỉ trạm',
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();

                    return (
                        <Box >
                            <Typography fontFamily={'Roboto Mono, Menlo'} textAlign={'center'} color={'black'} fontSize={'14px'} sx={{
                            }} >{cellValue}</Typography>
                        </Box>

                    );
                },


            },
            {
                accessorKey: 'status',
                header: 'Trạng thái',
                size: 150,
                Cell: ({ cell }) => {
                    let cellString = cell.getValue<number>();

                    return (
                        <Box alignItems={'center'} justifyContent={'center'} display={'flex'} >
                            <Tag color={cellString === 1 ? 'success' : 'error'}> {cellString === 1 ? "Hoạt động" : 'Ngừng hoạt động'} </Tag>
                        </Box>

                    );
                },


            },

        ],
        []
    );



    return (
        <>
            {contextHolder}

            <MaterialReactTable columns={columns} data={dataTable}
                state={{
                    isLoading: isLoading
                }}
                muiTableBodyRowProps={({ row }) => ({

                    sx: {
                        transition: 'background-color 0.3s', // Add a transition effect for smoother hover
                        '&:hover': {
                            filter: 'brightness(1.1)',
                        },
                        height: '6px',
                    },
                })}
                muiTableHeadCellProps={{
                    align: 'center'

                }}
                initialState={{
                    pagination: {
                        pageSize: 10,
                        pageIndex: 0,
                    },
                    density: 'compact',
                }}
                renderRowActionMenuItems={
                    ({ row, closeMenu }) => [
                        <MenuItem key="proceed" onClick={() => {
                            closeMenu()
                            handleUpdateStatusStaff(row.original.id, row.original.status)
                        }}>
                            {row.original.status === 1 ? 'Vô hiệu hóa tài khoản' : 'Kích hoạt tài khoản'}
                        </MenuItem>,
                        <MenuItem onClick={() => [
                            closeMenu()
                        ]} key="cancel">
                            Hủy
                        </MenuItem>
                    ]
                }
                enableRowActions={true}
            />



        </>
    );
};

export default DataTable;
