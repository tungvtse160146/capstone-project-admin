import React, { useEffect, useMemo, useState } from 'react';
import {
    Input,
    InputGroup,
    Table,
    DOMHelper,
    Progress,
    Checkbox,
    Stack,
    SelectPicker,
    Loader
} from 'rsuite';
import SearchIcon from '@rsuite/icons/Search';
import MoreIcon from '@rsuite/icons/legacy/More';
import axios from 'axios';
import { url } from '../../../url'
import { Box, MenuItem, Typography } from '@mui/material';
import { useDispatch } from 'react-redux';

import { province } from '../../../data/tinh_tp';
import { district } from '../../../data/quan_huyen';
import StationCard from './StationCard';
import { Pagination } from 'rsuite';
import { notification, Button, Empty } from 'antd'

type NotificationType = 'success' | 'error'

export type Station = {
    id: string,
    address: string,
    city: string | undefined,
    district: string | undefined,
    image_url: string,
    name: string,
    status: number,

}



const DataTable = () => {
    const [data, setData] = useState<any[]>([])
    const [isLoading, setIsLoading] = useState(false);
    const [dataTable, setDataTable] = useState<Station[]>([])
    const [activePage, setActivePage] = useState(1)
    const [currentLength, setCurrentLength] = useState(0)
    const [searchName, setSearchName] = useState<string>('');



    const [api, contextHolder] = notification.useNotification()



    const openNotificationWithIcon = (type: NotificationType, error?: string, message?: any) => {

        api[type]({
            message: 'Duyệt trạm của nhà chành',
            description: type === 'success' ? `${message} ` : 'Có lỗi xảy ra, vui lòng thực hiện lại!'
        })
    }




    const bearerToken = localStorage.getItem('sessionId')
    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`
    }

    const getStationList = async (i: number, name?: string) => {
        const params = {
            page: i,
            perPage: 10
        }
        setIsLoading(true)
        try {
            axios({

                method: 'get',
                url: `${url}/stations?filter[status]=1&filter[name]=${name || ""} `,
                headers: headers,
                params: params
            }).then((res) => {
                console.log(res, 'data');
                setCurrentLength(res.data.meta.total)
                setData(res.data.data)
                setIsLoading(false)


            }).catch((error) => {
                console.log(error);
                setIsLoading(false)

            })
        } catch (error) {
            console.log(error);
            setIsLoading(false)

        }
    }
    useEffect(() => {
        console.log(currentLength);

    }, [currentLength])

    React.useEffect(() => {
        let resData = data.map((item) => {
            return {
                id: item.id,
                address: item.attributes.address,
                city: province.find((city) => city.code === item.attributes.city_code.toString())?.name_with_type,
                district: district.find((district) => district.code === item.attributes.district_code.toString())?.name_with_type,
                image_url: item.attributes.avatar_url,
                name: item.attributes.name,
                status: item.attributes.status
            }
        })
        setDataTable(resData)
        console.log(resData);

    }, [data])

    React.useEffect(() => {
        getStationList(activePage)
    }, [activePage])

    const handleApproveStation = (id: string) => {
        console.log(id);

        try {
            axios({
                method: "PUT",
                headers: headers,
                url: `${url}/stations/${id}/approve`
            }).then((res) => {
                console.log(res);
                openNotificationWithIcon("success", '', 'Yêu cầu đã được chấp nhận!')
                getStationList(activePage, searchName)

            }).catch((error) => {
                console.log(error);
                openNotificationWithIcon("error")
            })
        } catch (error) {
            console.log(error);
            openNotificationWithIcon("error")

        }
    }

    const handleDenyStation = (id: string) => {
        try {
            axios({
                method: "PUT",
                headers: headers,
                url: `${url}/stations/${id}/deny`
            }).then((res) => {
                console.log(res);
                openNotificationWithIcon("success", '', 'Yêu cầu đã bị từ chối!')
                getStationList(activePage, searchName)

            }).catch((error) => {
                console.log(error);
                openNotificationWithIcon("error")

            })
        } catch (error) {
            console.log(error);
            openNotificationWithIcon("error")

        }
    }

    const handleSearchName = (searchName: string) => {
        getStationList(activePage, searchName)
    }




    return (
        <>
            {contextHolder}
            {!isLoading ? (<>
                <Box display={'flex'} gap="1.5rem" justifyContent={'center'} alignItems={'center'} mb="1rem">
                    <Input onChange={setSearchName} width={200} value={searchName} placeholder='Tìm kiếm theo tên trạm' />
                    <Button onClick={() => { handleSearchName(searchName) }} type='primary'> Tìm kiếm</Button>
                </Box>

                {dataTable.length ? dataTable.map((item: Station) => {
                    return (
                        <StationCard station={item} onApproveStation={handleApproveStation} onDenyStation={handleDenyStation} />
                    )
                }) : (<Empty description="Hiện tại không có yêu cầu nào!" />)}
                {dataTable.length ? (<Box justifyContent={'center'}  >
                    <Pagination prev last next first size="md" total={currentLength} limit={10} activePage={activePage} onChangePage={setActivePage} />

                </Box>) : (<></>)}
            </>) : (<Loader center content="Đang tải dữ liệu" />)}



        </>
    );
};

export default DataTable;
