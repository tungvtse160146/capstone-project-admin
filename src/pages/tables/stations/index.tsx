import React from 'react';
import { Breadcrumb, Panel } from 'rsuite';
import DataTable from './DataTable';

const StationTable = () => {
    return (
        <Panel
            header={
                <>
                    <h3 className="title">Danh sách trạm yêu cầu của nhà chành</h3>
                    <Breadcrumb>
                        <Breadcrumb.Item href="/">Trang chủ</Breadcrumb.Item>
                        <Breadcrumb.Item>Bảng</Breadcrumb.Item>
                        <Breadcrumb.Item active>Danh sách trạm yêu cầu của nhà chành</Breadcrumb.Item>
                    </Breadcrumb>
                </>
            }
        >
            <DataTable />
        </Panel>
    );
};

export default StationTable;
