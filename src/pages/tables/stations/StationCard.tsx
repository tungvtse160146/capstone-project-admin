import React from 'react'
import { Station } from './DataTable'
import { Panel, Placeholder, Stack, ButtonGroup, Button } from 'rsuite';
import { Box, Typography, useMediaQuery } from '@mui/material';
import { Tag } from 'antd';



const StationCard = ({ station, onApproveStation, onDenyStation }: { station: Station, onApproveStation: (id: string) => void, onDenyStation: (id: string) => void }) => {
    const {
        id,
        address,
        city,
        district,
        image_url,
        name,
        status
    } = station
    const isNonMobile = useMediaQuery("(min-width:600px)");


    const getStatus = (status: number) => {
        switch (status) {
            case 0: return "Đã được duyệt"
            case 1: return "Đang chờ duyệt"
            case 2: return "Đã xóa"
            case 3: return "Đã từ chối"
        }
    }
    return (
        <Panel
            style={{
                marginBottom: "1rem",
                backgroundColor: 'white'
            }}
            bordered
            header={
                <Stack justifyContent="space-between">
                    <Typography fontWeight={'bold'}>{name}</Typography>

                </Stack>
            }

        >
            <Box display='flex' justifyContent='flex-start' gap="1.5rem">
                <Box flexBasis="20%">
                    {image_url && <img style={{
                        width: 200, height: 150
                    }} src={image_url} alt="Report Image" />}
                </Box>

                <Box flexBasis={'60%'} display={'flex'} flexDirection={'column'} justifyContent={'space-between'} >
                    <Box display={'flex'} flexDirection={'column'} gap="0.5rem">
                        <Box display={'flex'} gap="0.25rem" >
                            <Typography fontWeight={'bold'}>Địa chỉ: </Typography>
                            <Typography>{address}</Typography>
                        </Box>
                        <Box display={'flex'} gap="0.25rem">
                            <Typography fontWeight={'bold'}>Tỉnh/thành phố: </Typography>
                            <Typography>{city}</Typography>
                        </Box>
                        <Box display={'flex'} gap="0.25rem">
                            <Typography fontWeight={'bold'}>Quận/huyện: </Typography>
                            <Typography>{district}</Typography>
                        </Box>
                        <Box display={'flex'} gap="0.25rem">
                            <Typography fontWeight={'bold'}>Trạng thái: </Typography>
                            <Tag color='processing'>{getStatus(status)}</Tag>
                        </Box>
                    </Box>

                </Box>
                <Box flexBasis={'20%'} display={'flex'} gap="0.5rem" justifyContent={'flex-end'} alignItems={'flex-end'}>
                    <Button onClick={() => { onApproveStation(id) }} color='blue' appearance="primary">
                        Phê duyệt
                    </Button>
                    <Button onClick={() => { onDenyStation(id) }} color='red' appearance="primary">
                        Từ chối
                    </Button>
                </Box>
            </Box>


        </Panel>
    )
}

export default StationCard