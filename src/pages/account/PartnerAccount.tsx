import React, { useEffect } from 'react';

import { Form, Button, Panel, InputGroup, Stack, Checkbox, Divider } from 'rsuite';
import EyeIcon from '@rsuite/icons/legacy/Eye';
import EyeSlashIcon from '@rsuite/icons/legacy/EyeSlash';
import { Link } from 'react-router-dom';
import Brand from '../../components/SideNav/Brand';
import axios from 'axios';
import { url } from '../../url'
import { Select, notification } from 'antd'
import { Typography } from '@mui/material';
import { bankList } from '../../data/bank';
import { object } from 'yup';

type NotificationType = 'success' | 'error'

const PartnerAccount = () => {
    const [visible, setVisible] = React.useState(false);
    const [name, setName] = React.useState('')
    const [username, setUsername] = React.useState('')
    const [accountName, setAccountName] = React.useState('')
    const [accountNumber, setAccountNumber] = React.useState('')
    const [bankCode, setBankCode] = React.useState('')
    const [isLoading, setIsLoading] = React.useState(false)
    const [formError, setFormError] = React.useState<any>();

    const [api, contextHolder] = notification.useNotification()

    const listRender: any[] = bankList.map((item) => {
        return {
            value: item.shortName,
            label: item.shortName
        }
    })


    const openNotificationWithIcon = (type: NotificationType, error?: any, data?: any) => {

        api[type]({
            message: 'Tạo tài khoản cho nhà chành',
            description: type === `success` ? `Tạo tài khoản thành công. Mật khẩu tự khởi tạo:  ${data.data.partner.generated_password}` : `${error}`
        })
    }

    useEffect(() => {
        if (name.length && username.length) {
            setFormError('')
        }
        else {
        }
    }, [name, username])


    const handleChangeUsername = (e: string) => {
        setUsername(e);

    }
    const handleChangeName = (e: string) => {
        setName(e)
    }
    const bearerToken = localStorage.getItem('sessionId')

    const headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${bearerToken}`
    }

    const handleSubmitForm = async () => {

        setIsLoading(true)

        const values = {
            name: name,
            username: username,
            bank_account_number: accountNumber,
            bank_account_name: accountName,
            bank_code: bankCode
        }
        axios({
            method: 'post',
            url: `${url}/account/partners`,
            data: JSON.stringify(values),
            headers: headers
        })
            .then((res) => {
                if (res.status === 200) {
                    setUsername('')
                    setName('')
                    setAccountName('')
                    setAccountNumber('')
                    setBankCode('')
                    openNotificationWithIcon('success', '', res.data)
                }
                setIsLoading(false)
            }).catch((error) => {

                if (error.response) {
                    console.log(error.response);
                    setFormError(error.response.data.message);
                    openNotificationWithIcon('error', error.response.data.message, '')

                } else if (error.request) {
                    console.log("No response received");
                    setFormError(500); // Default to a generic error status
                } else {
                    console.log("Error setting up the request", error.message);
                    setFormError(500); // Default to a generic error status
                }

            })
    }




    return (
        <>
            {contextHolder}
            <Stack
                justifyContent="center"
                alignItems="center"
                direction="column"
                style={{
                    height: '100vh'
                }}
            >
                <Brand style={{ marginBottom: 10 }} />
                <Panel
                    header={<h3>Tạo tài khoản cho nhà chành</h3>}
                    bordered
                    style={{ background: '#fff', width: 400 }}
                >


                    <Form fluid>
                        <Form.Group>
                            <Form.ControlLabel>Tài khoản</Form.ControlLabel>
                            <Form.Control required value={username} onChange={handleChangeUsername} name="username" />
                        </Form.Group>

                        <Form.Group>
                            <Form.ControlLabel>Tên</Form.ControlLabel>
                            <Form.Control type='name' required value={name} onChange={handleChangeName} name="name" />
                        </Form.Group>
                        <Form.Group>
                            <Form.ControlLabel>Số tài khoản</Form.ControlLabel>
                            <Form.Control required value={accountNumber} onChange={(e: string) => { setAccountNumber(e) }} name="accountNumber" />
                        </Form.Group>

                        <Form.Group>
                            <Form.ControlLabel>Tên tài khoản</Form.ControlLabel>
                            <Form.Control type='name' required value={accountName} onChange={(e: string) => { setAccountName(e) }} name="name" />
                        </Form.Group>
                        <Form.Group>
                            <Form.ControlLabel>Ngân hàng</Form.ControlLabel>
                            <Select style={{
                                width: 360
                            }} options={listRender} value={bankCode} onChange={(e: string) => setBankCode(e)} />
                        </Form.Group>

                        {
                            formError && (
                                <Typography mb="1rem" color="red">
                                </Typography>
                            )
                        }

                        <Form.Group>
                            <Stack spacing={6}>
                                <Button onClick={() => {
                                    if (name.length && username.length) {
                                        handleSubmitForm()
                                    }
                                    else {
                                        setFormError('Vui lòng điền đầy đủ thông tin')
                                    }
                                }} appearance="primary">Tạo tài khoản</Button>
                            </Stack>
                        </Form.Group>
                    </Form>
                </Panel>
            </Stack>

        </>
    );
};

export default PartnerAccount;
