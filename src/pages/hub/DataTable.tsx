import React, { useEffect, useMemo, useState } from "react";
import { Stack } from "rsuite";
import axios from "axios";
import { url } from "../../url";
import { Box, MenuItem, Typography } from "@mui/material";
import { MaterialReactTable, type MRT_ColumnDef } from "material-react-table";
import { Form, Input, Modal, Select, notification, Button } from "antd";

type NotificationType = "success" | "error";
type FieldType = {
    name: string;
    address: string;
    long: string;
    lat: string;
};
type Hub = {
    id: string;
    name: string;
    address: string;
};
const apiKey = "nRcGdKxeWpyKn2AJhpyerXIbhGKYARCp4jTJUgFh";
export const getCoordinates = async (location: string) => {
    try {
        if (!location) {
            return null;
        }
        const res = await axios.get(
            `https://rsapi.goong.io/geocode?address=${location}&api_key=${apiKey}`
        );
        return res.data.results[0].geometry.location;
    } catch (error) {
        // toast.error("Có lỗi xảy ra với google maps apis!");
        console.log(error);
        return error;
    }
};


const DataTable = () => {
    const [data, setData] = useState<any[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    const [listHub, setListHub] = useState<Hub[]>([]);
    const [open, setOpen] = useState(false);
    const [form] = Form.useForm();
    const [api, contextHolder] = notification.useNotification();
    const [optionAddress, setOptionAddress] = useState<string[]>([])
    const [address, setAddress] = useState<string>('')
    const openNotificationWithIcon = (
        type: NotificationType,
        error?: any,
        data?: any
    ) => {
        api[type]({
            message: "Tạo mới hub",
            description:
                type === `success`
                    ? `Tạo mới hub thành công`
                    : "Có lỗi xảy ra, vui lòng thao tác lại",
        });
    };

    const bearerToken = localStorage.getItem("sessionId");
    const headers = {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${bearerToken}`,
    };
    const handleGetCoordinate = async (e: any) => {
        console.log(e);
        setAddress(e)

        try {
            const res = await getCoordinates(e);
            form.setFieldsValue({
                long: res.lng,
                lat: res.lat
            })
        } catch (error) {
            console.log(error);
        }
    };

    const getHubList = async () => {
        try {
            axios({
                method: "get",
                url: `${url}/hubs`,
                headers: headers,
            })
                .then((res) => {
                    let hubs = res.data.data.map((item: any) => {
                        return {
                            id: item.id,
                            name: item.attributes.name,
                            address: item.attributes.address,
                        };
                    });
                    setListHub(hubs);
                })
                .catch((error) => {
                    console.log(error);
                });
        } catch (error) {
            console.log(error);
        }
    }

    const handleSubmitForm = async (values: FieldType) => {
        let newValues = {
            name: values.name,
            address: address,
            longitude: values.long,
            latitude: values.lat
        }

        try {
            axios({
                method: 'post',
                headers: headers,
                url: `${url}/hubs`,
                data: JSON.stringify(newValues)
            }).then((res) => {
                openNotificationWithIcon('success')
                setOpen(false)
                getHubList()
            })
        } catch (error) {
            openNotificationWithIcon('error')
        }
        form.resetFields()
    };


    React.useEffect(() => {
        getHubList()
    }, []);

    const handleGetLocation = async (value: any) => {
        try {
            if (!value) {
                return null;
            }
            const res = await axios.get(
                `https://rsapi.goong.io/Place/AutoComplete?api_key=${apiKey}&input=${value}&litmit=5`
            );

            setOptionAddress(res.data.predictions.map((item: any) => item.description))
        } catch (error) {
            console.log(error);
            return error;
        }
    }


    const columns = useMemo<MRT_ColumnDef<Hub>[]>(
        () => [
            {
                accessorKey: "id",
                header: "Id",
                size: 100,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();
                    return (
                        <Box>
                            <Typography
                                fontFamily={"Roboto Mono, Menlo"}
                                textAlign={"center"}
                                color={"black"}
                                fontSize={"14px"}
                                sx={{}}
                            >
                                {cellValue}
                            </Typography>
                        </Box>
                    );
                },
            },

            {
                accessorKey: "name",
                header: "Tên hub",
                size: 150,

                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();

                    return (
                        <Box>
                            <Typography
                                fontFamily={"Roboto Mono, Menlo"}
                                textAlign={"center"}
                                color={"black"}
                                fontSize={"14px"}
                                sx={{}}
                            >
                                {cellValue}
                            </Typography>
                        </Box>
                    );
                },
            },

            {
                accessorKey: "address",
                header: "Địa chỉ hub",
                size: 150,
                Cell: ({ cell }) => {
                    let cellValue = cell.getValue<string>();

                    return (
                        <Box>
                            <Typography
                                fontFamily={"Roboto Mono, Menlo"}
                                textAlign={"center"}
                                color={"black"}
                                fontSize={"14px"}
                                sx={{}}
                            >
                                {cellValue}
                            </Typography>
                        </Box>
                    );
                },
            },
        ],
        []
    );

    return (
        <>
            {contextHolder}
            <Stack
                className="table-toolbar"
                justifyContent="space-between"
                style={{
                    marginBottom: "1rem",
                }}
            >
                <Button
                    type="primary"
                    onClick={() => {
                        setOpen(true);
                    }}
                >
                    Thêm 1 hub
                </Button>
            </Stack>
            <MaterialReactTable
                columns={columns}
                data={listHub}
                state={{
                    isLoading: isLoading,
                }}
                muiTableBodyRowProps={({ row }) => ({
                    sx: {
                        transition: "background-color 0.3s", // Add a transition effect for smoother hover
                        "&:hover": {
                            filter: "brightness(1.1)",
                        },
                        height: "6px",
                    },
                })}
                muiTableHeadCellProps={{
                    align: "center",
                }}
                initialState={{
                    pagination: {
                        pageSize: 10,
                        pageIndex: 0,
                    },
                    density: "compact",
                }}
            />

            <Modal
                title={`Tạo mới 1 hub`}
                footer={false}
                onCancel={() => setOpen(false)}
                open={open}
                width={500}
            >
                <Form
                    form={form}
                    onFinish={handleSubmitForm}
                >
                    <p className="mb-1 ">Tên hub:</p>

                    <Form.Item
                        name={"name"}
                        rules={[
                            {
                                required: true,
                                message: "Tên hub không được bỏ trống.",
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <p className="mb-1 ">Địa chỉ:</p>
                    <Form.Item
                        name={"address"}
                        rules={[
                            {
                                required: true,
                                message: "Địa chỉ không được bỏ trống.",
                            },
                        ]}
                    >
                        <div
                            style={{
                                display: "flex",
                                gap: "5px",
                            }}
                        >
                            <Select showSearch onSearch={handleGetLocation} placeholder="Nhập địa chỉ" onSelect={handleGetCoordinate}
                                options={optionAddress.map((item) => {
                                    return {
                                        value: item,
                                        label: item
                                    }
                                })} />
                        </div>
                    </Form.Item>

                    <div style={{ display: "flex", gap: "10px" }}>
                        <div>
                            <p className="mb-1 ">Long:</p>
                            <Form.Item
                                name={"long"}
                                rules={[
                                    {
                                        required: true,
                                        message: "Tọa độ không được bỏ trống.",
                                    },
                                ]}
                            >
                                <Input disabled />
                            </Form.Item>
                        </div>
                        <div>
                            <p className="mb-1 ">Lat:</p>
                            <Form.Item
                                name={"lat"}
                                rules={[
                                    {
                                        required: true,
                                        message: "Tọa độ không được bỏ trống.",
                                    },
                                ]}
                            >
                                <Input disabled />
                            </Form.Item>
                        </div>
                    </div>
                    <div
                        style={{
                            width: "100%",
                            display: "flex",
                            justifyContent: "right",
                            height: "35px",
                        }}
                    >
                        <Form.Item>
                            <Button
                                type="primary"
                                className="bg-[#1677ff]"
                                htmlType="submit"
                                style={{ marginRight: "15px" }}
                            >
                                Xác nhận
                            </Button>
                        </Form.Item>

                        <Button
                            onClick={() => setOpen(false)}
                            className="bg-[#f72a2a] text-white"
                            type="default"
                        >
                            Hủy
                        </Button>
                    </div>
                </Form>
            </Modal>
        </>
    );
};

export default DataTable;
