import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    title: null
};

export const roleSlice = createSlice({
    name: 'title',

    initialState,
    reducers: {

        setTitle: (state, action) => {
            state.title = action.payload.title;
        },
    }
})



export const { setTitle } = roleSlice.actions;
export default roleSlice.reducer;