import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    submitForm: false
};

export const booleanSlice = createSlice({
    name: 'submit',

    initialState,
    reducers: {

        setSubmitForm: (state, action) => {
            state.submitForm = action.payload.submitForm;
        },
    }
})



export const { setSubmitForm } = booleanSlice.actions;
export default booleanSlice.reducer;