import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import roleReducer from './roleSlice'
import countReducer from './countSlice'
import submitFormReducer from './booleanSlice'
import titleReducer from './navSideSlice'
import { apiSlice } from '../services/apiSlice'

export const store = configureStore({
    reducer: {
        role: roleReducer,
        count: countReducer,
        submitForm: submitFormReducer,
        title: titleReducer,
        [apiSlice.reducerPath]: apiSlice.reducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(apiSlice.middleware)
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch