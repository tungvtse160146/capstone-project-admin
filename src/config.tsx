import React from 'react';
import { Icon } from '@rsuite/icons';
import { VscTable, VscCalendar } from 'react-icons/vsc';
import { MdFingerprint, MdDashboard, MdModeEditOutline } from 'react-icons/md';
import CubesIcon from '@rsuite/icons/legacy/Cubes';
import { RiFileList3Line, RiMoneyDollarCircleLine } from 'react-icons/ri'
import { AiOutlineSchedule } from 'react-icons/ai'
import { PiUsers } from "react-icons/pi";
import { FaLocationDot } from "react-icons/fa6";
import { MdRememberMe } from "react-icons/md";
import { BiHeadphone } from "react-icons/bi";
import { FaBox } from "react-icons/fa";
import { MdAddHomeWork } from "react-icons/md";



export const adminNavs = [
    {
        eventKey: 'dashboards',
        icon: <Icon as={MdDashboard} />,
        title: 'Số liệu thống kê',
        to: '/dashboards'
    },
    {
        eventKey: 'transactions',
        icon: <Icon as={RiMoneyDollarCircleLine} />,
        title: 'Danh sách giao dịch',
        children: [
            {
                eventKey: 'transactions',
                title: ' Giao dịch yêu cầu',
                to: '/transactions'
            },
            {
                eventKey: 'payments',
                title: 'Thanh toán khách hàng',
                to: '/payments'
            },
        ]
    },
    {
        eventKey: 'users',
        icon: <Icon as={PiUsers} />,
        title: 'Danh sách khách hàng',
        to: '/users'
    },

    {
        eventKey: 'partners',
        icon: <Icon as={MdRememberMe} />,
        title: 'Danh sách các nhà chành',
        to: '/partners'
    },

    {
        eventKey: 'orders',
        icon: <Icon as={RiFileList3Line} />,
        title: 'Danh sách đơn hàng',
        to: '/orders'
    },
    {
        eventKey: 'box-size',
        title: 'Danh sách box size',
        icon: <Icon as={FaBox} />,
        to: '/box-size',
    },
    {
        eventKey: 'hub',
        title: 'Danh sách hub',
        icon: <Icon as={FaBox} />,
        to: '/hubs'
    },
    {
        eventKey: 'staffs',
        icon: <Icon as={BiHeadphone} />,
        title: 'Danh sách nhân viên',
        to: '/staffs'
    },
    {
        eventKey: 'proceed-station',
        title: 'Danh sách trạm yêu cầu của nhà chành',
        icon: <Icon as={MdAddHomeWork} />,
        to: '/proceed-station',
    },
    {
        eventKey: 'create-partner-account',
        title: 'Tạo tài khoản cho nhà chành',
        icon: <Icon as={MdFingerprint} />,

        to: '/create-partner-account',
    },
    {
        eventKey: 'create-staff-account',
        title: 'Tạo tài khoản cho nhân viên',
        icon: <Icon as={MdFingerprint} />,

        to: '/create-staff-account',
    },


]

export const staffNavs = [



    {
        eventKey: 'transactions',
        icon: <Icon as={RiMoneyDollarCircleLine} />,
        title: 'Danh sách giao dịch',
        to: '/transactions'
    },

    // {
    //   eventKey: 'hubs',
    //   icon: <Icon as={FaLocationDot} />,
    //   title: 'Danh sách trạm',
    //   to: '/hubs'
    // },
    {
        eventKey: 'partners',
        icon: <Icon as={MdRememberMe} />,
        title: 'Danh sách nhà chành',
        to: '/partners'
    },

    {
        eventKey: 'orders',
        icon: <Icon as={RiFileList3Line} />,
        title: 'Danh sách đơn hàng',
        to: '/orders'
    },
    // {
    //   eventKey: 'box-size',
    //   title: 'Danh sách box size',
    //   icon: <Icon as={FaBox} />,

    //   to: '/box-size',
    // },


];
